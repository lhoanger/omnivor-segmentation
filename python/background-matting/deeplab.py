import os, cv2, sys, glob, argparse, time, tarfile
import numpy as np
from PIL import Image
import tensorflow as tf


class DeepLabModel(object):
	INPUT_TENSOR_NAME = 'ImageTensor:0'
	OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
	INPUT_SIZE = 513
	FROZEN_GRAPH_NAME = 'frozen_inference_graph'

	def __init__(self, tarball_path):
		self.graph = tf.Graph()
		graph_def = None
		tar_file = tarfile.open(tarball_path)
		for tar_info in tar_file.getmembers():
			if self.FROZEN_GRAPH_NAME in os.path.basename(tar_info.name):
				file_handle = tar_file.extractfile(tar_info)
				graph_def = tf.GraphDef.FromString(file_handle.read())
				break
		tar_file.close()

		if graph_def is None:
			raise RuntimeError('Cannot find inference graph in tar archive.')
		with self.graph.as_default():
			tf.import_graph_def(graph_def, name='')
		self.sess = tf.Session(graph=self.graph)

	def run(self, image):
		width, height = image.size
		resize_ratio = 1.0 * self.INPUT_SIZE / max(width, height)
		target_size = (int(resize_ratio * width), int(resize_ratio * height))
		resized_image = image.convert('RGB').resize(target_size, Image.ANTIALIAS)
		batch_seg_map = self.sess.run(
			self.OUTPUT_TENSOR_NAME,
			feed_dict={self.INPUT_TENSOR_NAME: [np.asarray(resized_image)]})
		seg_map = batch_seg_map[0]
		return resized_image, seg_map


def main(arguments):
  global args
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument('-i', '--input_dir', type=str, required=True , help='Directory of input *_comp.png images')
  parser.add_argument('-m', '--model', type=str, required=True, help='Name or path of model to load')
  parser.add_argument('--image_ext', type=str, default='_img.png', help='Extension of images to load')
  args = parser.parse_args(arguments)
  print(args)

  MODEL = DeepLabModel(args.model)
  print('model loaded successfully!')

  list_im = glob.glob(args.input_dir + '/*' + args.image_ext)
  list_im.sort()

  for i in range(0,len(list_im)):

    image = Image.open(list_im[i])
    print(list_im[i])
    start_time = time.time()

    res_im,seg=MODEL.run(image)

    seg = cv2.resize(seg.astype(np.uint8),image.size)
    print("Elapsed {} seconds".format(time.time() - start_time))

    mask_sel = (seg == 15).astype(np.float32)

    name = list_im[i].replace(args.image_ext,'_soft.png')
    cv2.imwrite(name, (255 * mask_sel).astype(np.uint8))

    img = cv2.imread(list_im[i])
    mask = cv2.imread(name,0)
    res = cv2.bitwise_and(img,img,mask = mask)
    fg_name = list_im[i].replace(args.image_ext,'_fgsoft.png')
    cv2.imwrite(fg_name, res)


  str_msg='\nDone: ' + args.input_dir
  print(str_msg)


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))

