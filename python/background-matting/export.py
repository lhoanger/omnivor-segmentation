import os, argparse, sys
import torch
import torch.nn as nn
from torch.autograd import Variable
from model import *


INPUT_SIZE = 512


def save_torch_script(checkpoint, out_file):
  model = Model()
  model = nn.DataParallel(model)
  param = torch.load(checkpoint)
  model.load_state_dict(param)
  model.cuda()
  del param
  I = Variable(torch.rand(1, 3, INPUT_SIZE, INPUT_SIZE).cuda())
  B = Variable(torch.rand(1, 3, INPUT_SIZE, INPUT_SIZE).cuda())
  S = Variable(torch.rand(1, 1, INPUT_SIZE, INPUT_SIZE).cuda())
  M = Variable(torch.rand(1, 4, INPUT_SIZE, INPUT_SIZE).cuda())

  # torch.jit does not support tracing a DataParallel module directly, so we need to trace its inner module
  # see: https://discuss.pytorch.org/t/how-to-trace-jit-in-evaluation-mode-using-multi-gpu-learned-model/33280
  module = torch.jit.trace(model.eval().module, [I, B, S, M])
  torch.jit.save(module, out_file)
  print('Saved torch.jit model to ' + out_file)


def main(arguments):
  global args
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument('--checkpoint', type=str, help='Path to the trained checkpoint file')
  parser.add_argument('--output', type=str, required=True, help='Path to the torchscript file to save')
  args = parser.parse_args(arguments)
  print(args)

  assert os.path.exists(args.checkpoint), 'Checkpoint not found: {}'.format(args.checkpoint)

  save_torch_script(args.checkpoint, args.output)


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
