from __future__ import print_function

import os, glob, time, argparse, cv2
#import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label

import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.backends.cudnn as cudnn

from model import *


def composite4(fg, bg, a):
	fg = np.array(fg, np.float32)
	alpha= np.expand_dims(a / 255,axis=2)
	im = alpha * fg + (1 - alpha) * bg
	im = im.astype(np.uint8)
	return im


def uncrop(alpha,bbox,R=720,C=1280):
  alpha=cv2.resize(alpha,(bbox[3],bbox[2]))
  if alpha.ndim==2:
    alpha_uncrop=np.zeros((R,C))
    alpha_uncrop[bbox[0]:bbox[0]+bbox[2],bbox[1]:bbox[1]+bbox[3]]=alpha
  else:
    alpha_uncrop=np.zeros((R,C,3))
    alpha_uncrop[bbox[0]:bbox[0]+bbox[2],bbox[1]:bbox[1]+bbox[3],:]=alpha
  return alpha_uncrop.astype(np.uint8)


def to_image(rec0):
  rec0=((rec0.data).cpu()).numpy()
  rec0=(rec0+1)/2
  rec0=rec0.transpose((1,2,0))
  rec0[rec0>1]=1
  rec0[rec0<0]=0
  return rec0


def create_bbox(bbox_init,sh):
  w=np.maximum(bbox_init[2],bbox_init[3])

  x1=bbox_init[0]-0.1*w
  y1=bbox_init[1]-0.1*w

  x2=bbox_init[0]+1.1*w
  y2=bbox_init[1]+1.1*w

  if x1<0: x1=0
  if y1<0: y1=0
  if x2>=sh[0]: x2=sh[0]-1
  if y2>=sh[1]: y2=sh[1]-1

  bbox=np.around([x1,y1,x2-x1,y2-y1]).astype('int')

  return bbox


def get_bbox(mask,R,C):
  where = np.array(np.where(mask))
  x1, y1 = np.amin(where, axis=1)
  x2, y2 = np.amax(where, axis=1)

  bbox_init=[x1,y1,np.maximum(x2-x1,y2-y1),np.maximum(x2-x1,y2-y1)]


  bbox=create_bbox(bbox_init,(R,C))

  return bbox

def crop_images(crop_list,reso,bbox):
  for i in range(0,len(crop_list)):
    img=crop_list[i]
    if img.ndim>=3:
      img_crop=img[bbox[0]:bbox[0]+bbox[2],bbox[1]:bbox[1]+bbox[3],...]; img_crop=cv2.resize(img_crop,reso)
    else:
      img_crop=img[bbox[0]:bbox[0]+bbox[2],bbox[1]:bbox[1]+bbox[3]]; img_crop=cv2.resize(img_crop,reso)
    crop_list[i]=img_crop
  return crop_list


torch.set_num_threads(1)
print('CUDA Device: ' + os.environ["CUDA_VISIBLE_DEVICES"])


"""Parses arguments."""
parser = argparse.ArgumentParser(description='Background Matting.')
parser.add_argument('-m', '--trained_model', type=str, help='Trained background matting model')
parser.add_argument('-o', '--output_dir', type=str, required=True,help='Directory to save the output results. (required)')
parser.add_argument('-i', '--input_dir', type=str, required=True,help='Directory to load input images. (required)')
parser.add_argument('-tb', '--target_back', type=str,help='Path to the target background image.')


args=parser.parse_args()

#input data path
data_path=args.input_dir

print('Using image mode')
#target background path
back_img10=cv2.imread(args.target_back); back_img10=cv2.cvtColor(back_img10,cv2.COLOR_BGR2RGB);
#Green-screen background
back_img20=np.zeros(back_img10.shape); back_img20[...,0]=120; back_img20[...,1]=255; back_img20[...,2]=155;



#initialize network
netM=Model()
netM=nn.DataParallel(netM)
netM.load_state_dict(torch.load(args.trained_model))
netM.cuda(); netM.eval()
cudnn.benchmark=True
reso=(512,512) #input reoslution to the network

#Create a list of test images
test_imgs = [f for f in os.listdir(data_path) if
         os.path.isfile(os.path.join(data_path, f)) and f.endswith('_img.png')]
test_imgs.sort()

#output directory
result_path=args.output_dir

if not os.path.exists(result_path):
  os.makedirs(result_path)

for i in range(0,len(test_imgs)):
  filename = test_imgs[i]
  print(filename)	
  bgr_img = cv2.imread(os.path.join(data_path, filename)); bgr_img=cv2.cvtColor(bgr_img,cv2.COLOR_BGR2RGB);
  bg_im0=cv2.imread(os.path.join(data_path, filename.replace('_img','_back'))); bg_im0=cv2.cvtColor(bg_im0,cv2.COLOR_BGR2RGB);

  #segmentation mask
  rcnn = cv2.imread(os.path.join(data_path, filename.replace('_img','_soft')),0);

  ## create the multi-frame
  multi_fr_w=np.zeros((bgr_img.shape[0],bgr_img.shape[1],4))
  multi_fr_w[...,0] = cv2.cvtColor(bgr_img,cv2.COLOR_BGR2GRAY);
  multi_fr_w[...,1] = multi_fr_w[...,0]
  multi_fr_w[...,2] = multi_fr_w[...,0]
  multi_fr_w[...,3] = multi_fr_w[...,0]

    
  start_time = time.time()

  #crop tightly
  bgr_img0=bgr_img;
  bbox=get_bbox(rcnn,R=bgr_img0.shape[0],C=bgr_img0.shape[1])

  crop_list=[bgr_img,bg_im0,rcnn,back_img10,back_img20,multi_fr_w]
  crop_list=crop_images(crop_list,reso,bbox)
  bgr_img=crop_list[0]; bg_im=crop_list[1]; rcnn=crop_list[2]; back_img1=crop_list[3]; back_img2=crop_list[4]; multi_fr=crop_list[5]

  #process segmentation mask
  kernel_er = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
  kernel_dil = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
  rcnn=rcnn.astype(np.float32)/255; rcnn[rcnn>0.2]=1;
  K=25

  zero_id=np.nonzero(np.sum(rcnn,axis=1)==0)
  del_id=zero_id[0][zero_id[0]>250]
  if len(del_id)>0:
    del_id=[del_id[0]-2,del_id[0]-1,*del_id]
    rcnn=np.delete(rcnn,del_id,0)
  rcnn = cv2.copyMakeBorder( rcnn, 0, K + len(del_id), 0, 0, cv2.BORDER_REPLICATE)


  rcnn = cv2.erode(rcnn, kernel_er, iterations=10)
  rcnn = cv2.dilate(rcnn, kernel_dil, iterations=5)
  rcnn=cv2.GaussianBlur(rcnn.astype(np.float32),(31,31),0)
  rcnn=(255*rcnn).astype(np.uint8)
  rcnn=np.delete(rcnn, range(reso[0],reso[0]+K), 0)


  #convert to torch
  img=torch.from_numpy(bgr_img.transpose((2, 0, 1))).unsqueeze(0); img=2*img.float().div(255)-1
  bg=torch.from_numpy(bg_im.transpose((2, 0, 1))).unsqueeze(0); bg=2*bg.float().div(255)-1
  rcnn_al=torch.from_numpy(rcnn).unsqueeze(0).unsqueeze(0); rcnn_al=2*rcnn_al.float().div(255)-1
  multi_fr=torch.from_numpy(multi_fr.transpose((2, 0, 1))).unsqueeze(0); multi_fr=2*multi_fr.float().div(255)-1


  with torch.no_grad():
    img,bg,rcnn_al, multi_fr =Variable(img.cuda()),  Variable(bg.cuda()), Variable(rcnn_al.cuda()), Variable(multi_fr.cuda())
    input_im=torch.cat([img,bg,rcnn_al,multi_fr],dim=1)
    
    fg_pred_tmp,alpha_pred=netM(img,bg,rcnn_al,multi_fr)
    
    al_mask=(alpha_pred>0.95).type(torch.cuda.FloatTensor)

    # for regions with alpha>0.95, simply use the image as fg
    fg_pred=img*al_mask + fg_pred_tmp*(1-al_mask)

    alpha_out=to_image(alpha_pred[0,...]); 

    #refine alpha with connected component
    labels=label((alpha_out>0.05).astype(int))
    try:
      assert( labels.max() != 0 )
    except:
      continue
    largestCC = labels == np.argmax(np.bincount(labels.flat)[1:])+1
    alpha_out=alpha_out*largestCC

    alpha_out=(255*alpha_out[...,0]).astype(np.uint8)				

    fg_out=to_image(fg_pred[0,...]); fg_out=fg_out*np.expand_dims((alpha_out.astype(float)/255>0.01).astype(float),axis=2); fg_out=(255*fg_out).astype(np.uint8)

    #Uncrop
    R0=bgr_img0.shape[0];C0=bgr_img0.shape[1]
    alpha_out0=uncrop(alpha_out,bbox,R0,C0)
    fg_out0=uncrop(fg_out,bbox,R0,C0)

  #compose
  back_img10=cv2.resize(back_img10,(C0,R0)); back_img20=cv2.resize(back_img20,(C0,R0))
  comp_im_tr1=composite4(fg_out0,back_img10,alpha_out0)

  print("Elapsed {} seconds".format(time.time() - start_time))

  comp_im_tr2=composite4(fg_out0,back_img20,alpha_out0)

  cv2.imwrite(result_path+'/'+filename.replace('_img','_out'), alpha_out0)
  cv2.imwrite(result_path+'/'+filename.replace('_img','_fg'), cv2.cvtColor(fg_out0,cv2.COLOR_BGR2RGB))
  cv2.imwrite(result_path+'/'+filename.replace('_img','_compose'), cv2.cvtColor(comp_im_tr1,cv2.COLOR_BGR2RGB))
  cv2.imwrite(result_path+'/'+filename.replace('_img','_matte').format(i), cv2.cvtColor(comp_im_tr2,cv2.COLOR_BGR2RGB))

  
  print('Done: ' + str(i+1) + '/' + str(len(test_imgs)))

