import torch
import torch.nn as nn
from torch.nn.modules.loss import _Loss
from torch.autograd import Variable
import numpy as np


class SupervisedLoss():
  def __init__(self):
    self.loss_l1 = L1Loss()
    self.loss_comp = CompositeLoss()
    self.loss_grad_alpha = GradAlphaLoss()
    self.loss_values = np.zeros(5)


  def reset(self):
    self.loss_values.fill(0)


  def compute(self, I, B, alpha, F, alpha_star, F_star):
    mask_none = Variable(torch.ones(alpha.shape).cuda())
    mask = (alpha > -0.99).type(torch.cuda.FloatTensor)
    alpha_loss = self.loss_l1(alpha, alpha_star, mask_none)
    grad_alpha_loss = self.loss_grad_alpha(alpha, alpha_star, mask_none)
    F_loss = self.loss_l1(F, F_star, mask)

    mask_pred = (alpha_star > 0.95).type(torch.cuda.FloatTensor)
    comp_pred = I * mask_pred + F_star * (1 - mask_pred)
    composite_loss = self.loss_comp(I, alpha_star, comp_pred, B, mask_none)

    loss = alpha_loss + grad_alpha_loss + 2 * F_loss + composite_loss

    self.loss_values += [loss.data.item(), alpha_loss.data.item(), grad_alpha_loss.data.item(), F_loss.data.item(),
      composite_loss.data.item()]

    return loss


  def report(self, nbatch):
    v = self.loss_values / nbatch
    return 'Loss = {:.4f} (Alpha = {:.4f}, Grad Alpha = {:.4f}, F = {:.4f}, Compose = {:.4f})'.format(
      v[0], v[1], v[2], v[3], v[4])


class L1Loss(_Loss):
  def __init__(self):
    super(L1Loss,self).__init__()

  def forward(self, gold, pred, mask):
    return L1Loss.compute(gold, pred, mask)

  @staticmethod
  def compute(gold, pred, mask):
    loss = 0
    N = gold.shape[0]
    for i in range(N):
      n_mask = mask[i, ...].sum()
      if n_mask > 0:
        loss += torch.sum(torch.abs(gold[i, ...] * mask[i, ...] - pred[i, ...] * mask[i, ...])) / (n_mask + 1e-6)
    return loss / N

    
class CompositeLoss(_Loss):
  def __init__(self):
    super(CompositeLoss,self).__init__()

  def forward(self, gold, alpha_pred, fore, back, mask):
    # model predicts alpha in [-1,1]
    alpha_pred = (alpha_pred + 1) / 2
    pred = fore * alpha_pred + (1 - alpha_pred) * back
    return L1Loss.compute(gold, pred, mask)


class GradAlphaLoss(_Loss):
  def __init__(self):
    super(GradAlphaLoss, self).__init__()

  def forward(self, gold, pred, mask):
    # https://en.wikipedia.org/wiki/Sobel_operator
    Gx = Variable(torch.Tensor([[1, 0, -1], [2, 0, -2], [1, 0, -1]]).view((1, 1, 3, 3)).cuda())
    gold_x = nn.functional.conv2d(gold, Gx, padding=1)
    pred_x = nn.functional.conv2d(pred, Gx, padding=1)
    loss_x = L1Loss.compute(gold_x, pred_x, mask)

    Gy = Variable(torch.Tensor([[1, 2, 1], [0, 0, 0], [-1, -2, -1]]).view((1, 1, 3, 3)).cuda())
    gold_y = nn.functional.conv2d(gold, Gy, padding=1)
    pred_y = nn.functional.conv2d(pred, Gy, padding=1)
    loss_y = L1Loss.compute(gold_y, pred_y, mask)

    return loss_x + loss_y