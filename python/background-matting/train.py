import os, sys, time, argparse, torch, torch.nn as nn, torch.optim as optim
from torch.autograd import Variable
from model import *
from dataset import *
from loss import *


def remove_none_examples(batch):
  # used to remove None examples from batch
  # refer to https://stackoverflow.com/questions/57815001/pytorch-collate-fn-reject-sample-and-yield-another
	batch = list(filter(lambda x: x is not None, batch))
	return torch.utils.data.dataloader.default_collate(batch)


def main(arguments):
  global args
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument('--dir', type=str, default=None, help='Directory containing training images')
  parser.add_argument('--bsz', type=int, default=4, help='Batch size')
  parser.add_argument('--epoch', type=int, default=60,help='# of training epochs')
  parser.add_argument('--progress_interval', type=int, default=50,help='# steps to report progress')
  parser.add_argument('--checkpoint', type=str, default='checkpoints', help='Directory to store checkpoints')
  args = parser.parse_args(arguments)
  print(args)

  model_dir = args.checkpoint
  if not os.path.exists(model_dir):
    os.mkdir(model_dir)

  model = Model()
  model.apply(init_parameters)
  model = nn.DataParallel(model)
  model.cuda()
  torch.backends.cudnn.benchmark=True

  supervised_loss = SupervisedLoss()
  optimizer = optim.Adam(model.parameters(), lr=1e-4)
  data = OmnivorData(dir=args.dir)
  data_loader = torch.utils.data.DataLoader(data, batch_size=args.bsz, shuffle=True, num_workers=args.bsz, 
    collate_fn=remove_none_examples)

  for epoch in range(args.epoch):
    model.train()

    supervised_loss.reset()
    nbatch = 0
    elapse_epoch = 0

    for _, data in enumerate(data_loader):
      step_start = time.time()

      I = Variable(data['I'].cuda())
      B = Variable(data['B'].cuda())
      S = Variable(data['S'].cuda())
      M = Variable(data['M'].cuda())
      F = Variable(data['F'].cuda())
      B_prime = Variable(data['B_prime'].cuda())
      alpha = Variable(data['alpha'].cuda())

      F_star, alpha_star = model(I ,B_prime, S, M)
      loss = supervised_loss.compute(I, B, alpha, F, alpha_star, F_star)
      optimizer.zero_grad()
      loss.backward()
      optimizer.step()

      step_end = time.time()

      elapse_step = step_end - step_start
      elapse_epoch += elapse_step
      nbatch += 1

      if nbatch % args.progress_interval == 0:
        loss_report = supervised_loss.report(nbatch)
        print('Epoch {:3d}, Step {:4d}: {}, Total Time = {:10.4f}s, Per Step = {:.4f}s'.format(
          epoch, nbatch, loss_report, elapse_epoch, elapse_step))
      del F, B, alpha, I, alpha_star, F_star, S, M

    epoch_loss = supervised_loss.loss_values[0]
    torch.save(model.state_dict(), model_dir + '/model_{}_{:.4f}.pth'.format(epoch, epoch_loss / nbatch))
    torch.save(optimizer.state_dict(), model_dir + '/optim_{}_{:.4f}.pth'.format(epoch, epoch_loss / nbatch))


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
