import torch, cv2, skimage
from skimage import io
from glob import glob
import numpy as np
from torch.utils.data import Dataset
import math


# different extensions for foreground, background, etc.. images
FORE_EXT = '_fore.png'
BACK_EXT = '_back.png'
COMP_EXT = '_comp.png'
MASK_EXT = '_alpha.png'
SOFT_EXT = '_soft.png'

# various preprocessing parameters
GAUSSIAN_MU_LO = -7
GAUSSIAN_MU_HI = 7
GAUSSIAN_SIG_LO = 2
GAUSSIAN_SIG_HI = 6
GAMMA_MU = 1
GAMMA_SIG = 0.12
MAX_ROTATE_ANGLE = math.pi / 6
MAX_TRANSLATE = 20


class OmnivorData(Dataset):
  def __init__(self, dir, max_size = 512):
    self.image_names = [p.replace(FORE_EXT, '') for p in glob(dir + '/*' + FORE_EXT)]
    self.max_size = max_size
    
  def __len__(self):
    return len(self.image_names)

  def __getitem__(self,idx):
    try:
      random_flip = np.random.random() > 0.5
      upsize = int(self.max_size * 1.5)
      cropsize = np.random.randint(self.max_size, upsize + 1)
      croprow, cropcol = np.random.randint(0, upsize - cropsize, 2) if cropsize < upsize else (0, 0)

      F = self.preprocess(self.image_names[idx] + FORE_EXT, upsize, cropsize, croprow, cropcol, random_flip)
      alpha = self.preprocess(self.image_names[idx] + MASK_EXT, upsize, cropsize, croprow, cropcol, random_flip, True)
      I = self.preprocess(self.image_names[idx] + COMP_EXT, upsize, cropsize, croprow, cropcol, random_flip)
      B = self.preprocess(self.image_names[idx] + BACK_EXT, upsize, cropsize, croprow, cropcol, random_flip)
      S = self.preprocess(self.image_names[idx] + SOFT_EXT, upsize, cropsize, croprow, cropcol, random_flip, True)

      B_prime = self.generate_B_prime(B)
      M = self.generate_M(F, B, alpha)

      instance = {
        'I': self.to_normalized_tensor(I),
        'F': self.to_normalized_tensor(F), 
        'alpha': self.to_normalized_tensor(alpha),
        'B': self.to_normalized_tensor(B), 
        'B_prime': self.to_normalized_tensor(B_prime), 
        'S': self.to_normalized_tensor(S),
        'M': self.to_normalized_tensor(M)
      }

      if False: # print diagnostics
        cv2.imwrite('{}_image.png'.format(idx), I)
        cv2.imwrite('{}_fg.png'.format(idx), F)
        cv2.imwrite('{}_back.png'.format(idx), B)
        cv2.imwrite('{}_alpha.png'.format(idx), alpha)
        cv2.imwrite('{}_seg.png'.format(idx), S)
        for c in range(4):
          cv2.imwrite('{}_M_{}.png'.format(idx, c), M[...,c])
        foo()

      return instance
    except Exception as e:
      print(e)


  def preprocess(self, img_path, upsize, cropsize, croprow, cropcol, flip=False, gray_scale=False):
    if gray_scale:
      img = cv2.imread(img_path, 0)
    else:
      img = cv2.imread(img_path)
      img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    img = cv2.resize(img, dsize=(upsize, upsize))
    if flip:
      img = cv2.flip(img, 1)

    if cropsize < upsize:
      img = img[croprow : croprow + cropsize, cropcol : cropcol + cropsize]
    
    if cropsize > self.max_size:
      img = cv2.resize(img, dsize=(self.max_size, self.max_size))

    return img


  def generate_composite(self, fore, back, alpha):
    unit_alpha = np.expand_dims(alpha / 255.0, axis=2)
    im = unit_alpha * fore + (1 - unit_alpha) * back
    return im.astype(np.uint8)


  def generate_B_prime(self, bg):
    if np.random.random() > 0.5:
      return self.add_gaussian_noise(bg)
    else:
      return skimage.exposure.adjust_gamma(bg, np.random.normal(GAMMA_MU, GAMMA_SIG))


  def generate_M(self, fg, bg, alpha, n_frames = 4):
    height, width = fg.shape[:2]
    M = np.zeros((height, width, n_frames))
    for c in range(n_frames):
      # Refer to https://medium.com/uruvideo/dataset-augmentation-with-random-homographies-a8f4b44830d4
      theta = (np.random.random() * 2 - 1) * MAX_ROTATE_ANGLE
      T_x = (np.random.random() * 2 - 1) * MAX_TRANSLATE
      T_y = (np.random.random() * 2 - 1) * MAX_TRANSLATE
      H_e = np.array([[np.cos(theta), -np.sin(theta), T_x],
                      [np.sin(theta),  np.cos(theta), T_y],
                      [0,              0,             1]])
      s_x = np.random.random() / 2
      s_y = np.random.random() / 2
      H_a = np.array([[1, s_y, 0],
                      [s_x, 1, 0],
                      [0,   0, 1]])
      H = H_e * H_a
      tfg = cv2.warpPerspective(fg.astype(np.uint8), H, (width, height), flags=cv2.INTER_LINEAR)
      talpha = cv2.warpPerspective(alpha.astype(np.uint8), H, (width, height), flags=cv2.INTER_NEAREST)
      M[:, :, c] = cv2.cvtColor(self.generate_composite(tfg, bg, talpha), cv2.COLOR_BGR2GRAY)
    return M


  # def generate_M(self, fg, bg, alpha, n_frames = 4):
  #   height, width = fg.shape[:2]
  #   M = np.zeros((height, width, n_frames))
  #   for t in range(n_frames):
  #     translate = np.random.normal(0,5, (2,1))
  #     theta = np.deg2rad(np.random.normal(0,7))
  #     rotate = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
  #     sc = np.array([[1+np.random.normal(0,0.05), 0], [0,1]])
  #     sh = np.array([[1, np.random.normal(0,0.05) * (np.random.random_sample() > 0.5)],
  #                    [np.random.normal(0,0.05) * (np.random.random_sample() > 0.5), 1]])
  #     A = np.concatenate((sc * sh * rotate, translate), axis=1)

  #     tfg = cv2.warpAffine(fg.astype(np.uint8), A, (width,height), flags=cv2.INTER_LINEAR,
  #       borderMode=cv2.BORDER_REFLECT)
  #     talpha = cv2.warpAffine(alpha.astype(np.uint8), A, (width,height), flags=cv2.INTER_NEAREST,
  #       borderMode=cv2.BORDER_REFLECT)

  #     noisy_bg = self.add_gaussian_noise(bg)

  #     M[..., t] = cv2.cvtColor(self.generate_composite(tfg, noisy_bg, talpha), cv2.COLOR_BGR2GRAY)
  #   return M


  def add_gaussian_noise(self, img):
    mu = np.random.randint(GAUSSIAN_MU_LO, GAUSSIAN_MU_HI + 1)
    sigma = np.random.randint(GAUSSIAN_SIG_LO, GAUSSIAN_SIG_HI + 1)
    h, w, c = img.shape
    err = np.random.normal(mu, sigma, (h, w, c))
    return np.clip(img.astype(np.float32) + err, 0, 255).astype(np.uint8)


  def to_normalized_tensor(self, img):
    if len(img.shape) >= 3:
      img = torch.from_numpy(img.transpose((2, 0, 1))) # convert to c x h x w
    else:
      img = torch.from_numpy(img).unsqueeze(0)
    img = 2 * (img.float().div(255)) - 1 # normalize to [-1, 1] range
    return img

