import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
import numpy as np


def init_parameters(m):
	classname = m.__class__.__name__
	if classname.find('Conv') != -1:
		init.xavier_uniform_(m.weight, gain=np.sqrt(2))
		if m.bias is not None:
			init.constant_(m.bias, 0)

	if classname.find('Linear') != -1:
		init.normal_(m.weight)
		init.constant_(m.bias, 1)

	if classname.find('BatchNorm2d') != -1:
		init.normal_(m.weight.data, 1.0, 0.2)
		init.constant_(m.bias.data, 0.0)


class Encoder(nn.Module):
  def __init__(self, num_channels):
    super(Encoder, self).__init__()

    self.block1 = nn.Sequential(
      nn.Conv2d(in_channels=num_channels, out_channels=64, kernel_size=7, stride=1, padding=3, bias=False),
      nn.BatchNorm2d(num_features=64),
      nn.ReLU(inplace=True),
      nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=2, padding=1, bias=False),
      nn.BatchNorm2d(num_features=128),
      nn.ReLU(inplace=True))

    self.block2 = nn.Sequential(
      nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, stride=2, padding=1, bias=False),
      nn.BatchNorm2d(num_features=256),
      nn.ReLU(inplace=True))


  def forward(self, input):
    b1 = self.block1(input) # this is used for image feature skip connection
    return b1, self.block2(b1)


class Selector(nn.Module):
  def __init__(self):
    super(Selector, self).__init__()

    self.block = nn.Sequential(
      nn.Conv2d(in_channels=512, out_channels=64, kernel_size=1, stride=1, padding=0, bias=False),
      nn.BatchNorm2d(num_features=64),
      nn.ReLU(inplace=True))


  def forward(self, image_feature, prior_feature):
    cat_feature = torch.cat([image_feature, prior_feature], dim=1)
    return self.block(cat_feature)


class Combinator(nn.Module):
  def __init__(self):
    super(Combinator, self).__init__()

    self.block = nn.Sequential(
      nn.Conv2d(in_channels=448, out_channels=256, kernel_size=1, stride=1, padding=0, bias=False),
      nn.BatchNorm2d(num_features=256),
      nn.ReLU(inplace=True))


  def forward(self, image_feature, back_selection, soft_selection, motion_selection):
    comb_feature = torch.cat([image_feature, back_selection, soft_selection, motion_selection], dim=1)
    return self.block(comb_feature)


class ResBLK(nn.Module):
  def __init__(self, num_blocks):
    super(ResBLK, self).__init__()

    blocks = []
    for _ in range(num_blocks):
      blocks += [nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1, bias=False),
        nn.BatchNorm2d(num_features=256),
        nn.ReLU(inplace=True),
        nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1, bias=False),
        nn.BatchNorm2d(num_features=256)]
    
    self.blocks = nn.Sequential(*blocks)


  def forward(self, input):
    return self.blocks(input)


class DecoderForeground(nn.Module):
  def __init__(self):
    super(DecoderForeground, self).__init__()

    self.cu128k3_block = nn.Sequential(
      nn.Upsample(scale_factor=2, mode='bilinear', align_corners = True),
      nn.Conv2d(in_channels=256, out_channels=128, kernel_size=3, stride=1, padding=1, bias=False),
      nn.BatchNorm2d(num_features=128),
      nn.ReLU(inplace=True))

    self.cu64k3_block = nn.Sequential(
      nn.Upsample(scale_factor=2, mode='bilinear', align_corners = True),
      nn.Conv2d(in_channels=256, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
      nn.BatchNorm2d(num_features=64),
      nn.ReLU(inplace=True))

    self.co3k7_block = nn.Conv2d(in_channels=64, out_channels=3, kernel_size=7, stride=1, padding=3, bias=False)


  def forward(self, image_feature, resblk_feature):
    first_output = self.cu128k3_block(resblk_feature)
    skip_input = torch.cat([image_feature, first_output], dim=1)
    output = self.cu64k3_block(skip_input)
    return self.co3k7_block(output)


class DecoderAlpha(nn.Module):
  def __init__(self):
    super(DecoderAlpha, self).__init__()

    self.block = nn.Sequential(
      nn.Upsample(scale_factor=2, mode='bilinear', align_corners = True),
      nn.Conv2d(in_channels=256, out_channels=128, kernel_size=3, stride=1, padding=1, bias=False),
      nn.BatchNorm2d(num_features=128),
      nn.ReLU(inplace=True),
      nn.Upsample(scale_factor=2, mode='bilinear', align_corners = True),
      nn.Conv2d(in_channels=128, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
      nn.BatchNorm2d(num_features=64),
      nn.ReLU(inplace=True),
      nn.Conv2d(in_channels=64, out_channels=1, kernel_size=7, stride=1, padding=3, bias=False),
      nn.Tanh())


  def forward(self, input):
    return self.block(input)


class Model(nn.Module):
  def __init__(self):
    super(Model, self).__init__()
    self.I_enc = Encoder(num_channels=3)
    self.B_enc = Encoder(num_channels=3)
    self.S_enc = Encoder(num_channels=1)
    self.M_enc = Encoder(num_channels=4)

    self.IB_sel = Selector()
    self.IS_sel = Selector()
    self.IM_sel = Selector()

    self.comb = Combinator()

    self.resblks7 = ResBLK(num_blocks=7)
    self.resblks3_fg = ResBLK(num_blocks=3)
    self.resblks3_al = ResBLK(num_blocks=3)

    self.decoder_fg = DecoderForeground()
    self.decoder_al = DecoderAlpha()
  

  def forward(self, I, B, S, M):
    skip_I_f, I_f = self.I_enc(I)
    _, B_f = self.B_enc(B)
    _, S_f = self.S_enc(S)
    _, M_f = self.M_enc(M)

    IB = self.IB_sel(I_f, B_f)
    IS = self.IS_sel(I_f, S_f)
    IM = self.IM_sel(I_f, M_f)

    combination = self.comb(I_f, IB, IS, IM)
    res_common = self.resblks7(combination)
    res_fg = self.resblks3_fg(res_common)
    res_al = self.resblks3_al(res_common)

    fg = self.decoder_fg(skip_I_f, res_fg)
    al = self.decoder_al(res_al)

    return fg, al

