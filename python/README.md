# Omnivor Segmentation Pipeline
These scripts perform image segmentation using the [Background Matting](http://grail.cs.washington.edu/projects/background-matting/) model and the [DeepLabv3+](https://github.com/tensorflow/models/tree/master/research/deeplab) model. 

## EC2 Setup
1. Choose the `Deep Learning AMI (Ubuntu 18.04) Version 35.0` image
2. Deploy to an instance with GPU, such as `p3.2xlarge`
3. Run the following in the current directory of this README:
```
    conda create --clone tensorflow_p36 --name integration
    conda activate integration
    pip install -r integration-requirements.txt
    cd ../../back-matting/tensorflow_models/research/
    export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
```
4. To process a video, run: `sh integration-run.sh [VIDEO_PATH] [RGB_VIDEO_PATH] [BACKGROUND_PATH] [OUTPUT_DIRECTORY]`, where:
    - `[VIDEO_PATH]`: path to the original video that's in NV12 format
    - `[RGB_VIDEO_PATH]`: path to the RGB video to be created
    - `[BACKGROUND_PATH]`: path to the NV12-format background image
    - `[OUTPUT_DIRECTORY]`: directory where ouput images and segmentation images are created, if not exists it will be created
5. The output images that are created are:
    - `*_img.png`: the RGB video frames
    - `*_back.png`: the background images
    - `*_out.png`: the Background-Matting segmentation masks
    - `*_fg.png`: the Background-Matting foreground images
    - `*_masksDL.png`: the DeepLabv3+ segmentation masks
    - `*.foreground.png`: the DeepLabv3+ foreground images
    

## Model Details & Methodology

Pretrained models are included in this zip file. Training methodology is as follows:

- Dataset: a new dataset was created which consists of supervisely-person images composed over omnivor's studio background images. TODO: add link to dataset
- DeepLabv3+: located at `back-matting/Models/deeplab_softseg_trainval_k21p15_os8_s10k_ftbn0.tar.gz`. This model was obtained by finetuning the tensorflow's [xception65_coco_voc_trainval](https://github.com/tensorflow/models/blob/master/research/deeplab/g3doc/model_zoo.md) checkpoint on omnivor dataset.
- Background Matting: located at `back-matting/Models/sly_omnivor_train/netG_epoch_59.pth`. This model is trained from scratch using the same dataset above, and using the output from finetuned DeepLabv3+ model as soft segmentation input.

