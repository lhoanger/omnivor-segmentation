VIDEO_PATH=$1
RGB_VIDEO_PATH=$2
BG_PATH=$3
OUT_PATH=$4

ffmpeg -i $VIDEO_PATH -filter:v "crop=2448:3072:0:0" -pix_fmt gray -vsync 0 -f rawvideo - | ffmpeg -y -pix_fmt nv12 -f rawvideo -s 2448x2048 -framerate 30 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 2 -i - -c:v libx265 -crf 15 -x265-params range=full -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 2 -vsync 0 $RGB_VIDEO_PATH

python preprocess-video.py --video $RGB_VIDEO_PATH --video_rgb --background $BG_PATH --out_dir $OUT_PATH --rotate 3 --crop_width 2448 --crop_height 3072
cd ../../back-matting/
python test_segmentation_deeplab.py -i $OUT_PATH -m Models/deeplab_softseg_trainval_k21p15_os8_s10k_ftbn0.tar.gz
python test_background-matting_image.py -m sly_omnivor_train -i $OUT_PATH -o $OUT_PATH -tb sample_data/background/0001.png