import cv2
import os, sys
import argparse
from tqdm import tqdm
import util
import numpy as np


def crop_image(img, row, col, width, height):
  h, w = img.shape[:2]

  assert row >= 0 and row < h
  assert col >= 0 and col < w

  width = w - col if width == 0 else width
  height = h - row if height == 0 else height

  assert row + height <= h
  assert col + width <= w

  return img[row:row+height, col:col+width]


def rotate_image(img, rotate):
  rotate = rotate % 4
  if rotate > 0:
    if rotate == 1:
      r = cv2.ROTATE_90_CLOCKWISE
    elif rotate == 2:
      r = cv2.ROTATE_180
    else:
      r = cv2.ROTATE_90_COUNTERCLOCKWISE
    return cv2.rotate(img, r)
  else:
    return img


def rescale_convert_NV12_BGR(img):
  fimg = img.astype(np.float)
  h, w = img.shape[:2]
  oh = int(h * 2 / 3)
  fimg[:oh,:] = (235 - 16) * fimg[:oh,:] / 255 + 16 # rescale luma region
  fimg[oh:, :] = (240 - 16) * fimg[oh:, :] / 255 + 16 # rescale chroma region
  return cv2.cvtColor(fimg.astype(np.uint8), cv2.COLOR_YUV2BGR_NV12) 


def process_image(image, row, col, width, height, rotate, resize_max, video_rgb):
  image = crop_image(image, row, col, width, height)
  if not video_rgb:
    image = rescale_convert_NV12_BGR(image)
  image = rotate_image(image, rotate)
  image = util.resize_image_to_max_size(image, resize_max, resize_max)
  return image


def main(arguments):
  global args
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument('--video', type=str, default=None, help='path to the input video')
  parser.add_argument('--video_rgb', action='store_true', help='specify if the video frames are in rgb mode')
  parser.add_argument('--background', type=str, default=None, help='path to the background image, which is always \
    required to be in NV12 format')
  parser.add_argument('--out_dir', type=str, default=None, help='output directory')
  parser.add_argument('--resize_max', type=int, default=512, help='max size (width or height) of image to resize to')
  parser.add_argument('--crop_row', type=int, default=0, help='row idx of the NV12-format crop region (before rotate)')
  parser.add_argument('--crop_col', type=int, default=0, help='column idx of the NV12-format crop region')
  parser.add_argument('--crop_width', type=int, default=2448, help='the width of the NV12-format crop region')
  parser.add_argument('--crop_height', type=int, default=3072, help='the height of the NV12-format crop region, if \
    video is in rgb mode, the crop height is adjusted by 2/3')
  parser.add_argument('--rotate', type=int, default=3, help='values: {0,1,2,3}, e.g. 1 = rotate 90 degrees clockwise')

  args = parser.parse_args(arguments)
  print(args)

  assert args.rotate in {0,1,2,3}, 'Invalid rotation parameter, accepted values are {0,1,2,3}'
  assert args.crop_row >= 0 and args.crop_col >= 0 and args.crop_width >=  0 and args.crop_height >= 0, 'Invalid crop'
  assert os.path.exists(args.video), 'Input video {} not found'.format(args.video)
  assert os.path.exists(args.background), 'Background image {} not found'.format(args.background)
  assert args.resize_max > 0
  assert args.crop_height % 3 == 0, 'The crop region is expected to be in NV12-format, where the height is 3/2 of the \
    original image height'

  if not os.path.exists(args.out_dir):
    os.mkdir(args.out_dir)

  bg = cv2.imread(args.background, 0)
  bg = process_image(bg, args.crop_row, args.crop_col, args.crop_width, args.crop_height, args.rotate, args.resize_max,
    False)

  video_name = os.path.splitext(os.path.basename(args.video))[0]
  vc = cv2.VideoCapture(args.video)
  success, image = vc.read()
  count = 0
  pbar = tqdm()
  while success:
    if not args.video_rgb:
      image = image[:,:,0]
    else:
      crop_height = int(args.crop_height * 2 / 3)
    image = process_image(image, args.crop_row, args.crop_col, args.crop_width, crop_height, args.rotate,
      args.resize_max, args.video_rgb)
    base_name = '{}_{}'.format(video_name, str(count + 1).zfill(9))
    cv2.imwrite(os.path.join(args.out_dir, '{}_img.png'.format(base_name)), image)
    cv2.imwrite(os.path.join(args.out_dir, '{}_back.png'.format(base_name)), bg)
    success, image = vc.read()
    count += 1
    pbar.update(1)
  pbar.close()


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
