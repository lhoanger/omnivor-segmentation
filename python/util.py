import cv2


def resize_image_to_max_size(img, max_width, max_height):
  '''
  Resize an image to the specified max dimensions.

  @param img: An nd-array image
  @param max_width: Max width to resize to
  @param max_height: Max height to resize to
  @return: A resized nd-array image 
  '''
  ih, iw = img.shape[:2]
  resize_ratio = min(max_width / iw, max_height / ih)
  nw = int(iw * resize_ratio)
  nh = int(ih * resize_ratio)
  return cv2.resize(img, (nw, nh))