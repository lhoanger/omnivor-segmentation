from numpy.testing._private.utils import assert_equal
import supervisely_lib as sly  # Supervisely Python SDK
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image
import cv2, glob
import os, sys
from random import shuffle
from shutil import copyfile


BASE_DIR = 'g:/data/datasets/supervisely-person-dataset'
VOC_DIR = 'g:/data/datasets/VOC2012'
OMNIVOR_BACKGROUND_DIR = 'g:/data/datasets/omnivor-background'
OMNIVOR_SUPERVISELY_COMPOSITION_DIR = 'g:/data/datasets/omnivor-supervisely-datasets'
OMNIVOR_SUPERVISELY_PASCALVOC_DIR = 'g:/data/datasets/pascalvoc-omnivor-supervisely-datasets'
OMNIVOR_SUPERVISELY_PASCALVOC_SMALLFG_DIR = 'g:/data/datasets/pascalvoc-omnivor-supervisely-datasets-smallfg'
OMNIVOR_SUPERVISELY_PASCALVOC_IMAGE_DIR = 'g:/data/datasets/pascalvoc-omnivor-supervisely-datasets/JPEGImages'
OMNIVOR_SUBJECT_DIR = 'g:/data/datasets/omnivor-subject-images'
OMNIVOR_VOC2012_DIR = 'g:/data/datasets/voc2012-omnivor-datasets'
OMNIVOR_VOC2012_SLYPER_DIR = 'g:/data/datasets/voc2012-slyper-omnivor-dataset'
BACK_MATTING_DIR = 'g:/data/datasets/backmatting-slyomnivor-dataset'


def test_mask_is_voc21class(dir):
  print('TEST: mask images must be in VOC2012 21-class format')
  masks = glob.glob(dir + '/*.png')
  labels = set()
  for mask_path in masks:
    slymask = cv2.imread(mask_path, 0)
    mlabels = np.unique(slymask)
    for ml in mlabels:
      labels.add(ml)
  for l in labels:
    if l != 255 and l > 20:
      print('TEST FAILED: mask images contain invalid pixel values.')
      exit()
  print('TEST PASSED.')


def get_mask_threshold(mask_path):
  ext = os.path.splitext(mask_path)[1]
  # for jpeg images, decompression may cause 0 pixel values to become larger than 0
  # use 127 to filter out the decompression artifacts
  return (127 if ext == '.jpeg' or ext == '.jpg' else 0)


# load an image and annotation file from the supervisely person dataset and display
def sly_show_img_mask_contours(ds_name, img_name):
  # Open existing project on disk.
  project = sly.Project(BASE_DIR, sly.OpenMode.READ)
  # Locate and load image labeling data.
  item_paths = project.datasets.get(ds_name).get_item_paths(img_name)
  ann = sly.Annotation.load_json_file(item_paths.ann_path, project.meta)
  # Go over the labeled objects and print out basic properties.
  for label in ann.labels:
    print('Found label object: ' + label.obj_class.name)
    print('   geometry type: ' + label.geometry.geometry_name())
    print('   object area: ' + str(label.geometry.area))

  # Read the underlying raw image for display.
  img = sly.image.read(item_paths.img_path)
  # Render the labeled objects.
  ann_render = np.zeros(ann.img_size + (3,), dtype=np.uint8)
  ann.draw(ann_render)
  # mask = cv2.cvtColor(ann_render, cv2.COLOR_BGR2GRAY)
  # mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)[1]
  # fore = cv2.bitwise_and(img, img, mask=mask)

  # Separately, render the labeled objects contours.
  ann_contours = np.zeros(ann.img_size + (3,), dtype=np.uint8)
  ann.draw_contour(ann_contours, thickness=7)

  matplotlib.rcParams['interactive'] = True

  plt.figure(figsize=(15, 15))
  plt.subplot(1, 3, 1)
  plt.imshow(img)
  plt.subplot(1, 3, 2)
  plt.imshow(ann_render)
  plt.subplot(1, 3, 3)
  plt.imshow(ann_contours)
  input("Press Enter to continue...")


# save all supervisely-person annotated masks to images
def sly_generate_mask_images():
  project = sly.Project(BASE_DIR, sly.OpenMode.READ)
  for dsk in project.datasets:
    ds = project.datasets.get(dsk.name)

    mask_dir = os.path.join(ds.directory, 'mask')
    if not os.path.exists(mask_dir):
      os.mkdir(mask_dir)

    for img_name in ds:
      print('{}/{}'.format(dsk.name, img_name))
      item_paths = ds.get_item_paths(img_name)
      mask_path = os.path.join(mask_dir, img_name)
      if os.path.exists(mask_path):
        print('Skipped {}: already exists'.format(mask_path))
        continue
      ann = sly.Annotation.load_json_file(item_paths.ann_path, project.meta)
      ann_render = np.zeros(ann.img_size + (3,), dtype=np.uint8)
      ann.draw(ann_render)
      im = Image.fromarray(ann_render)
      im.save(mask_path)
      print('\t->\t{}'.format(mask_path))


# save all supervisely-person annotated masks to images
def sly_generate_foreground_images():
  project = sly.Project(BASE_DIR, sly.OpenMode.READ)
  print(project.meta)

  for dsk in project.datasets:
    ds = project.datasets.get(dsk.name)

    fg_dir = os.path.join(ds.directory, 'fg')
    if not os.path.exists(fg_dir):
      os.mkdir(fg_dir)

    for img_name in ds:
      print('{}/{}'.format(dsk.name, img_name))
      item_paths = ds.get_item_paths(img_name)
      fore_path = os.path.join(fg_dir, img_name)
      if os.path.exists(fore_path):
        print('Skipped {}: already exists'.format(fore_path))
        continue
      ann = sly.Annotation.load_json_file(item_paths.ann_path, project.meta)
      ann_render = np.zeros(ann.img_size + (3,), dtype=np.uint8)
      ann.draw(ann_render)
      
      mask = ann_render[:, :, 0]
      mask[mask > 0] = 255
      img = cv2.imread(item_paths.img_path)
      fore = cv2.bitwise_and(img, img, mask=mask)
      cv2.imwrite(fore_path, fore)
      print('\t->\t{}'.format(fore_path))


def sly_print_image_dimensions_stats():
  w = []
  h = []
  r = []
  for f in glob.glob(BASE_DIR + '/**/img/*.png'):
    im = Image.open(f)
    width, height = im.size
    w.append(width)
    h.append(height)
    r.append(float(width) / height)

  print('Width: max = {}, min = {}, avg = {}'.format(np.max(w), np.min(w), np.average(w)))
  print('Height: max = {}, min = {}, avg = {}'.format(np.max(h), np.min(h), np.average(h)))
  print('Ratio: max = {}, min = {}, avg = {}'.format(np.max(r), np.min(r), np.average(r)))
    

def get_mask_bounding_box_and_fg(img_mask):
  rows, cols = img_mask.shape
  # get mask bounding box and foreground ratio
  bb_left = bb_right = bb_upper = bb_lower = -1
  num_fg = 0
  for r in range(rows):
    for c in range(cols):
      if img_mask[r,c] != 0:
        num_fg += 1
        bb_left = c if bb_left == -1 else min(bb_left, c)
        bb_upper = r if bb_upper == -1 else min(bb_upper, r)
        bb_right = max(bb_right, c)
        bb_lower = max(bb_lower, r)

  return (num_fg, bb_left, bb_right, bb_upper, bb_lower)


# Create a new image by composing the foreground of one image over another background image
# The foreground is taken by applying the specified mask to the specified image
# min_fg_ratio: skip if the fg ratio in the mask is less than this value
# resize_fg_min: resize so that fg ratio is at least this much
# resize_fg_max: resize so that fg ratio is at most this much
# ignore_mask_value: if non zero, any pixel in the mask with this value is set to 0 (background)
# read_mask_with_cv2: if True, use cv2.imread to read mask images, otherwise use PIL.Image
# NOTE: set resize_fg_max to 1.0 to skip resizing of the mask and foreground subjects
# NOTE: set out_mask_path to None to skip saving resized versions
def compose_image_to_new_bg(img_path, mask_path, bg_path, out_mask_path,
  min_fg_ratio = 0, resize_fg_min = 0.2, resize_fg_max = 0.4, show_result = False,
  ignore_mask_value = 0, read_mask_with_cv2 = True):

  mask_threshold = get_mask_threshold(mask_path)
  im_mask = cv2.imread(mask_path, 0) if read_mask_with_cv2 else np.array(Image.open(mask_path))
  if ignore_mask_value > 0:
    im_mask[im_mask == ignore_mask_value] = 0

  im_mask = cv2.threshold(im_mask, mask_threshold, 255, cv2.THRESH_BINARY)[1]

  num_fg, bb_left, bb_right, bb_upper, bb_lower = get_mask_bounding_box_and_fg(im_mask)
  # skip if too little foreground
  fg_ratio = num_fg / im_mask.size
  if fg_ratio < min_fg_ratio:
    print('Skipped {}, too little foreground (fg/bg) = {}'.format(img_path, fg_ratio))
    return None

  im_orig = cv2.imread(img_path)
  im_back = cv2.imread(bg_path)
  h, w = im_mask.shape

  im_orig = cv2.resize(im_orig, (w, h))
  im_back = cv2.resize(im_back, (w, h))

  im_fore = cv2.bitwise_and(im_orig, im_orig, mask = im_mask)
  if fg_ratio > resize_fg_max:
    # resize the mask and foreground image to smaller size
    new_fg_ratio = np.random.uniform(low = resize_fg_min, high = resize_fg_max)
    fg_scale = new_fg_ratio / fg_ratio
    nw = int(fg_scale * w)
    nh = int(fg_scale * h)
    scaled_im_fore = cv2.resize(im_fore, (nw, nh))
    scaled_im_mask = cv2.resize(im_mask, (nw, nh))
    scaled_im_mask = cv2.threshold(scaled_im_mask, mask_threshold, 255, cv2.THRESH_BINARY)[1]

    # determine a random location in the original size to place the resized mask and foreground
    rand_row = np.random.randint(0, h - nh)
    rand_col = np.random.randint(0, w - nw)
    im_mask.fill(0)
    im_mask[rand_row : rand_row + nh, rand_col : rand_col + nw] = scaled_im_mask
    
    im_fore.fill(0)
    im_fore[rand_row : rand_row + nh, rand_col : rand_col + nw] = scaled_im_fore
    print('Resized mask and foreground from ({},{}), ratio = {} to ({},{}), ratio = {}'.format(
      w, h, fg_ratio, nw, nh, new_fg_ratio
    ))

  if out_mask_path is not None:
    # write new mask out to file
    cv2.imwrite(out_mask_path, im_mask)

  im_mask_inv = cv2.bitwise_not(im_mask)
  im_back = cv2.bitwise_and(im_back, im_back, mask = im_mask_inv)
  
  im_done = cv2.add(im_fore, im_back)
  if show_result:
    cv2.imshow('res', im_done)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
  return im_done


# Create a synthetic dataset by composing the supervisely foreground subjects over the background images
# in the specified directory
def sly_create_dataset_new_bg(bg_dir, out_dir):
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)
  
  resized_mask_dir = os.path.join(out_dir, 'mask')
  if not os.path.exists(resized_mask_dir):
    os.mkdir(resized_mask_dir)

  out_img_dir = os.path.join(out_dir, 'img')
  if not os.path.exists(out_img_dir):
    os.mkdir(out_img_dir)

  bg_images = glob.glob(bg_dir + '/*.png')

  project = sly.Project(BASE_DIR, sly.OpenMode.READ)
  for dsk in project.datasets:
    ds = project.datasets.get(dsk.name)

    mask_dir = os.path.join(ds.directory, 'mask')
    
    for img_name in ds:
      print('{}/{}'.format(dsk.name, img_name))
      item_paths = ds.get_item_paths(img_name)
      img_path = item_paths.img_path
      mask_path = os.path.join(mask_dir, img_name)
      resized_mask_path = os.path.join(resized_mask_dir, img_name)
      bg_path = bg_images[np.random.randint(0, len(bg_images))]
      new_img = compose_image_to_new_bg(img_path, mask_path, bg_path, resized_mask_path)
      if new_img is None:
        continue
      new_img_path = os.path.splitext(img_name)[0] + '_' + os.path.splitext(os.path.basename(bg_path))[0] + '.png'
      new_img_path = os.path.join(out_img_dir, new_img_path)
      print('Saving to {}'.format(new_img_path))
      cv2.imwrite(new_img_path, new_img)


def resize_image_to_max_size(img, max_width, max_height):
  iw, ih = img.size
  resize_ratio = min(max_width / iw, max_height / ih)
  nw = int(iw * resize_ratio)
  nh = int(ih * resize_ratio)
  return img.resize((nw, nh), Image.ANTIALIAS)


def convert_slyomnivor_dataset_to_pascalvoc_format(img_dir, out_dir, resize_max_image_dim = 512, train_split = .7):
  pascalvoc_img_ext = '.jpg'
  pascalvoc_ann_ext = '.png'
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)
  pascalvoc_img_path = os.path.join(out_dir, 'JPEGImages')
  pascalvoc_ann_path = os.path.join(out_dir, 'SegmentationClass')
  pascalvoc_seg_path = os.path.join(out_dir, 'ImageSets/Segmentation')
  os.mkdir(pascalvoc_img_path)
  os.mkdir(pascalvoc_ann_path)
  os.makedirs(pascalvoc_seg_path)

  img_masks = glob.glob(img_dir + '/mask/*.*')
  name_to_img_mask = {}
  for i in range(len(img_masks)):
    mask_name = os.path.splitext(os.path.basename(img_masks[i]))[0]
    name_to_img_mask[mask_name] = img_masks[i]

  train_seg = []
  val_seg = []
  mapping = []
  idx = 0
  for img_path in glob.glob(img_dir + '/img/*.png'):
    img_name = os.path.basename(img_path).split('_CV')[0]
    mask_path = name_to_img_mask[img_name]
    print(img_path + '\t' + mask_path)

    img = Image.open(img_path)
    mask = Image.open(mask_path)

    iw, ih = img.size
    mw, mh = mask.size
    if iw != mw or ih != mh:
      print('image {} and mask must have the same dimensions, ({},{}) != ({},{})'.format(img_name, iw, ih, mw, mh))
      sys.exit()

    resize_ratio = min(resize_max_image_dim / iw, resize_max_image_dim / ih)
    nw = int(iw * resize_ratio)
    nh = int(ih * resize_ratio)

    img = resize_image_to_max_size(img, resize_max_image_dim, resize_max_image_dim)
    mask = resize_image_to_max_size(mask, resize_max_image_dim, resize_max_image_dim)

    # save image and annotation files
    img_id = str(idx).zfill(6)
    img.save(os.path.join(pascalvoc_img_path, img_id + pascalvoc_img_ext))
    mask.save(os.path.join(pascalvoc_ann_path, img_id + pascalvoc_ann_ext))

    # create train/val split
    if np.random.rand() <= train_split:
      train_seg.append(img_id)
    else:
      val_seg.append(img_id)

    mapping.append((img_id, os.path.basename(img_path)))
    idx += 1

  train_val_seg = train_seg + val_seg
  shuffle(train_val_seg)

  with open(os.path.join(pascalvoc_seg_path, 'train.txt'), 'w') as f:
    for item in train_seg:
      f.write("%s\n" % item)

  with open(os.path.join(pascalvoc_seg_path, 'val.txt'), 'w') as f:
    for item in val_seg:
      f.write("%s\n" % item)

  with open(os.path.join(pascalvoc_seg_path, 'trainval.txt'), 'w') as f:
    for item in train_val_seg:
      f.write("%s\n" % item)

  with open(os.path.join(out_dir, 'mapping.txt'), 'w') as f:
    for item in mapping:
      f.write("{}\t{}\n".format(item[0], item[1]))


# Add omnivor test set images to the existing train/val dataset 
# Also creates dummy all-zero mask images under ../SegmentationClass
# Also creates a file under ../ImageSets/Segmentation/omnivor.txt 
# Also appends to ../mapping.txt 
# Note: This method should be called after convert_slyomnivor_dataset_to_pascalvoc_format()
# Note: out_dir must already exist and if there're any existing images, the file names must be in '000000.jpg' format.
#       New images will have names incremented from the highest existing file name index.
def create_omnivor_testset_in_pascalvoc_format(img_dir, out_dir, resize_max_image_dim = 512):
  existing_images = sorted(glob.glob(out_dir + '/*.jpg'))
  file_index = int(os.path.splitext(os.path.basename(existing_images[-1]))[0]) + 1
  print('New images will be created with file names starting from index {}'.format(file_index))

  mapping = []
  file_names = []
  images = glob.glob(img_dir + '/*.png')
  for img_path in images:
    img = Image.open(img_path)
    img = img.convert('RGB')
    img = resize_image_to_max_size(img, resize_max_image_dim, resize_max_image_dim)
    file_name = str(file_index).zfill(6)
    out_path = os.path.join(out_dir, '{}.jpg'.format(file_name)) # .jpg for images in pascalvoc format
    img.save(out_path)
    print('Saved image to {}'.format(out_path))

    dummy_mask_data = np.array(img)[:,:,0] # 1d image
    dummy_mask_data.fill(0)
    dummy_mask_img = Image.fromarray(dummy_mask_data)
    dummy_mask_path = os.path.join(out_dir, '../SegmentationClass/{}.png'.format(file_name)) # .png for mask
    dummy_mask_img.save(dummy_mask_path)
    print('Saved dummy mask to {}'.format(dummy_mask_path))

    file_names.append(file_name)
    mapping.append((file_name, os.path.basename(img_path)))
    file_index += 1

  with open(os.path.join(out_dir, '../ImageSets/Segmentation/omnivor.txt'), 'w') as sf:
    for ind in file_names:
      sf.write('{}\n'.format(ind))

  with open(os.path.join(out_dir, '../mapping.txt'), 'a') as mf:
    for tup in mapping:
      mf.write('{}\t{}\n'.format(tup[0], tup[1]))


# for testing purposes: generate foreground images and binary (0, or 255) segmentation masks 
def create_omnivor_foreground_and_rawseg_images(dataset_dir):
  img_dir = os.path.join(dataset_dir, 'JPEGImages')
  raw_dir = os.path.join(dataset_dir, 'SegmentationClassRaw')
  fore_dir = os.path.join(dataset_dir, 'Foreground')
  if not os.path.exists(raw_dir):
    os.mkdir(raw_dir)
  if not os.path.exists(fore_dir):
    os.mkdir(fore_dir)

  # construct a mapping from image name to absolute path
  img_name_to_path = {}
  project = sly.Project(BASE_DIR, sly.OpenMode.READ)
  for dsk in project.datasets:
    ds = project.datasets.get(dsk.name)
    for img_name in ds:
      img_name_no_ext = os.path.splitext(img_name)[0]
      img_name_to_path[img_name_no_ext] = (dsk.name, img_name)

  # read mapping from index to image name
  mapping_file = os.path.join(dataset_dir, 'mapping.txt')
  idx_to_img = {} 
  with open(mapping_file, 'r') as mf:
    for line in mf:
      parts = line.strip().split('\t')
      idx_to_img[parts[0]] = parts[1]

  for img_path in glob.glob(img_dir + '/*.jpg'):
    img = cv2.imread(img_path)
    h,w = img.shape[:-1]

    img_name = os.path.basename(img_path)
    img_idx = os.path.splitext(img_name)[0]
    original_image_name = idx_to_img[img_idx]
    name_and_bg = original_image_name.split('_CV')
    if len(name_and_bg) <= 1: # omnivor test images don't have background images
      continue
    
    img_name_no_ext = name_and_bg[0]
    ds_name, sly_img_name = img_name_to_path[img_name_no_ext]
    print('Loading mask from {}/{}'.format(ds_name, sly_img_name))
    ds = project.datasets.get(ds_name)
    item_paths = ds.get_item_paths(sly_img_name)
    ann = sly.Annotation.load_json_file(item_paths.ann_path, project.meta)
    ann_render = np.zeros(ann.img_size + (3,), dtype=np.uint8)
    ann.draw(ann_render)
    seg = ann_render[:, :, 0]
    seg[seg > 0] = 255
    seg = cv2.resize(seg, (w,h), interpolation=cv2.INTER_NEAREST)
    seg_path = os.path.join(raw_dir, img_idx + '.png')
    cv2.imwrite(seg_path, seg)

    fore = cv2.bitwise_and(img, img, mask = seg)
    fore_path = os.path.join(fore_dir, img_idx + '.png')
    cv2.imwrite(fore_path, fore)


def create_omnivor_person_segmentation_dataset():
  # first compose the supervisely foreground subjects over the omnivor background images
  sly_create_dataset_new_bg(OMNIVOR_BACKGROUND_DIR, OMNIVOR_SUPERVISELY_COMPOSITION_DIR)

  # then convert the dataset to pascalvoc format
  convert_slyomnivor_dataset_to_pascalvoc_format(
    OMNIVOR_SUPERVISELY_COMPOSITION_DIR, OMNIVOR_SUPERVISELY_PASCALVOC_DIR)

  # then add test set images (also in pascalvoc format)
  create_omnivor_testset_in_pascalvoc_format(OMNIVOR_SUBJECT_DIR, OMNIVOR_SUPERVISELY_PASCALVOC_IMAGE_DIR)

  # generate foreground images only for visual testing
  create_omnivor_foreground_and_rawseg_images(OMNIVOR_SUPERVISELY_PASCALVOC_DIR)


def extract_omnivor_foreground(original_img_dir, mask_dir, mapping_file):
  mask_files = glob.glob(mask_dir + '/b*.png')

  mapping = {}
  with open(mapping_file, 'r') as mf:
    for line in mf:
      tup = line.strip().split('\t')
      mapping[tup[0]] = tup[1]

  for i in range(len(mask_files)):
    file_index = os.path.basename(mask_files[i])[2:8]
    original_file_name = mapping[file_index]
    original_img_path = os.path.join(original_img_dir, original_file_name)
    img = cv2.imread(original_img_path)
    mask = cv2.imread(mask_files[i], 0)
    h,w = img.shape[:-1]

    mask = cv2.resize(mask.astype(np.uint8), (w, h))
    mask_sel = (mask == 15).astype(np.float32)
    mask = (255 * mask_sel).astype(np.uint8)
    fg = cv2.bitwise_and(img, img, mask = mask)
    fg_path = os.path.join(mask_dir, original_file_name)
    cv2.imwrite(fg_path, fg)
    cv2.imwrite(fg_path.replace('.png', '_masksDL.png'), mask)


def extract_omnivor_voc12_foreground(original_img_dir, mask_dir):
  mask_files = glob.glob(mask_dir + '/b*.png')

  for i in range(len(mask_files)):
    original_file_name = os.path.basename(mask_files[i])[2:].replace("'", '')
    original_img_path = os.path.join(original_img_dir, original_file_name)
    img = cv2.imread(original_img_path)
    mask = cv2.imread(mask_files[i], 0)
    h,w = img.shape[:-1]
    mask[mask != 15] = 0
    mask[mask == 15] = 255
    mask = cv2.resize(mask.astype(np.uint8), (w, h))
    mask = cv2.threshold(mask, 254, 255, cv2.THRESH_BINARY)[1]
    fg = cv2.bitwise_and(img, img, mask = mask)
    fg_path = os.path.join(mask_dir, original_file_name)
    cv2.imwrite(fg_path, fg)


# intersect two images which only contain foreground subject and black (zero) background
# for all locations, if any pixel in either image is zero then the output is also zero 
def intersect_two_foreground_images(dir1, dir2, out_dir):
  images1 = [os.path.basename(im) for im in glob.glob(dir1 + '/*.png')]
  images2 = [os.path.basename(im) for im in glob.glob(dir2 + '/*.png')]
  common_images = [im for im in images1 if im in images2]

  for img_name in common_images:
    img1 = cv2.imread(os.path.join(dir1, img_name))
    img2 = cv2.imread(os.path.join(dir2, img_name))
    assert_equal(img1.shape, img2.shape, 'images are not of the same size')

    out_img = np.copy(img1)
    for r in range(img1.shape[0]):
      for c in range(img1.shape[1]):
        if np.count_nonzero(img1[r,c]) == 0 or np.count_nonzero(img2[r,c]) == 0:
          out_img[r,c,:] = 0

    print(os.path.join(out_dir, img_name))
    cv2.imwrite(os.path.join(out_dir, img_name), out_img)


def pascalvoc_generate_foreground_images(dir):
  out_dir = os.path.join(dir, 'Foreground')
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)

  jpeg_images = glob.glob(os.path.join(dir, 'JPEGImages') + '/*.jpg')
  mask_images = glob.glob(os.path.join(dir, 'SegmentationClass') + '/*.png')
  for img_path in jpeg_images:
    file_name = os.path.basename(img_path)
    mask_path = os.path.join(dir, 'SegmentationClass/' + file_name.replace('.jpg', '.png'))
    if not os.path.exists(mask_path):
      print('Skipped {}, mask path {} does not exist'.format(img_path, mask_path))
      continue
    img = cv2.imread(img_path)
    mask = cv2.imread(mask_path, 0)
    mask = cv2.threshold(mask, get_mask_threshold(mask_path), 255, cv2.THRESH_BINARY)[1] # TODO: 127 or 0?
    # mask[mask > 0] = 1

    fg = cv2.bitwise_and(img, img, mask = mask)
    fg_path = out_dir + '/' + file_name
    print('Saving to {}'.format(fg_path))
    cv2.imwrite(fg_path, fg)


# if images in the synthetic omnivor dataset are manually removed for quality control
# then this method can be run to clean up paths in the dataset splits that reference 
# those dead images
def sly_omnivor_pascalvoc_clean_dead_paths(dataset_split_dir):
  split_files = glob.glob(dataset_split_dir + '/*.txt')

  for sf in split_files:
    images = []
    n_dead_paths = 0
    with open(sf, 'r') as sff:
      for line in sff:
        image_name = line.strip()
        image_path = os.path.join(dataset_split_dir, '../../JPEGImages/' + image_name + '.jpg')
        mask_path = os.path.join(dataset_split_dir, '../../SegmentationClass/' + image_name + '.png')
        if not os.path.exists(image_path) or not os.path.exists(mask_path):
          print('Image file {} does not exist, removing from dataset split {}'.format(os.path.basename(sf)))
          n_dead_paths += 1
        else:
          images.append(image_name)
    
    if n_dead_paths > 0:
      print('Found {} dead paths from split {}, files remaining: {}'.format(
        n_dead_paths, os.path.basename(sf), len(images)))
      with open(sf, 'w') as sfw:
        for im in images:
          sfw.write('{}\n'.format(im))


# Create a synthetic dataset by composing the VOC2012 foreground subjects over the background images
# in the specified directory
# NOTE: does not create segmentation images, rather these should be copied over from the original dataset
def voc12_create_dataset_new_bg(dir, bg_dir, out_dir, omnivor_test_dir, train_split = .7):
  pascalvoc_img_ext = '.jpg'
  pascalvoc_ann_ext = '.png'
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)
  in_pascalvoc_img_path = os.path.join(dir, 'JPEGImages')
  in_pascalvoc_ann_path = os.path.join(dir, 'SegmentationClass')

  out_pascalvoc_img_path = os.path.join(out_dir, 'JPEGImages')
  out_pascalvoc_ann_path = os.path.join(out_dir, 'SegmentationClass')
  out_pascalvoc_seg_path = os.path.join(out_dir, 'ImageSets/Segmentation')
  if not os.path.exists(out_pascalvoc_img_path):
    os.mkdir(out_pascalvoc_img_path)
  if not os.path.exists(out_pascalvoc_ann_path):
    os.mkdir(out_pascalvoc_ann_path)
  if not os.path.exists(out_pascalvoc_seg_path):
    os.makedirs(out_pascalvoc_seg_path)

  input_images = glob.glob(in_pascalvoc_img_path + '/*' + pascalvoc_img_ext)
  bg_images = glob.glob(bg_dir + '/*.png')

  train_seg = []
  val_seg = []
  bg_mapping = []

  print('Creating main dataset consisting of VOC12 foreground subjects')
  for img_path in input_images:
    img_name = os.path.basename(img_path)
    mask_name = img_name.replace(pascalvoc_img_ext, pascalvoc_ann_ext)
    mask_path = os.path.join(in_pascalvoc_ann_path, mask_name)
    out_mask_path = os.path.join(out_pascalvoc_ann_path, mask_name)
    if not os.path.exists(mask_path): # skip unsegmented images
      print('Skipped {}, no segmentation found'.format(img_path))
      continue

    bg_path = bg_images[np.random.randint(0, len(bg_images))]
    new_img = compose_image_to_new_bg(img_path, mask_path, bg_path, out_mask_path=None, 
      resize_fg_max=1, ignore_mask_value=255, read_mask_with_cv2=False)
    if new_img is None:
      continue
    new_img_path = os.path.join(out_pascalvoc_img_path, img_name)
    print('Saving to {}'.format(new_img_path))
    cv2.imwrite(new_img_path, new_img)
    # copy mask file out
    copyfile(mask_path, out_mask_path)

    # create train/val split
    img_id = os.path.splitext(img_name)[0]
    if np.random.rand() <= train_split:
      train_seg.append(img_id)
    else:
      val_seg.append(img_id)

    # store background mapping
    bg_mapping.append((img_name, os.path.basename(bg_path)))
  
  train_val_seg = train_seg + val_seg
  shuffle(train_val_seg)

  with open(os.path.join(out_pascalvoc_seg_path, 'train.txt'), 'w') as f:
    for item in train_seg:
      f.write("%s\n" % item)

  with open(os.path.join(out_pascalvoc_seg_path, 'val.txt'), 'w') as f:
    for item in val_seg:
      f.write("%s\n" % item)

  with open(os.path.join(out_pascalvoc_seg_path, 'trainval.txt'), 'w') as f:
    for item in train_val_seg:
      f.write("%s\n" % item)

  with open(os.path.join(out_dir, 'background_mapping.txt'), 'w') as f:
    for item in bg_mapping:
      f.write('{}\t{}\n'.format(item[0], item[1]))

  print('Creating Omnivor testset')
  omnivor_set = []
  omnivor_images = glob.glob(omnivor_test_dir + '/*.png')
  for oim in omnivor_images:
    oim_name = os.path.splitext(os.path.basename(oim))[0]
    omnivor_set.append(oim_name)

    oimg = Image.open(oim)
    oimg = oimg.convert('RGB')
    oimg = resize_image_to_max_size(oimg, 500, 500)
    out_oimg_path = os.path.join(out_pascalvoc_img_path, oim_name + '.jpg')
    oimg.save(out_oimg_path)

    dummy_mask_data = np.array(oimg)[:,:,0] # 1d image
    dummy_mask_data.fill(0)
    dummy_mask_img = Image.fromarray(dummy_mask_data)
    dummy_mask_path = os.path.join(out_pascalvoc_ann_path, oim_name + '.png')
    dummy_mask_img.save(dummy_mask_path)

  with open(os.path.join(out_pascalvoc_seg_path, 'omnivor.txt'), 'w') as f:
    for item in omnivor_set:
      f.write("%s\n" % item)


def slyomnivor_convert_mask_to_voc21class(in_dir, out_dir):
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)
  slyom_masks = glob.glob(in_dir + '/*.png')
  for slymask_path in slyom_masks:
    slymask = cv2.imread(slymask_path, 0)
    slymask[slymask > 0] = 15
    cv2.imwrite(os.path.join(out_dir, os.path.basename(slymask_path)), slymask)
  test_mask_is_voc21class(out_dir)


def voc12omnivor_convert_mask_to_voc21class(in_dir, out_dir):
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)
  v12om_masks = glob.glob(in_dir + '/*.png')
  for v12mask_path in v12om_masks:
    v12mask = np.array(Image.open(v12mask_path))
    v12mask = Image.fromarray(v12mask.astype(dtype=np.uint8))
    v12mask.save(os.path.join(out_dir, os.path.basename(v12mask_path)), 'PNG')
  test_mask_is_voc21class(out_dir)


# create train.txt, trainval.txt, and val.txt dataset splits
# the voc12-supervisely-person-omnivor dataset is assumed to be already created
def create_voc12_slyper_omnivor_data_splits(img_dir, train_split = 0.7):
  images = glob.glob(img_dir  + '/JPEGImages/*.jpg')
  image_names = []
  for img in images:
    img_name = os.path.splitext(os.path.basename(img))[0]
    if img_name.startswith('CV'): # ignore omnivor images
      continue
    image_names.append(img_name)
  shuffle(image_names)
  num_train = int(train_split * len(image_names))
  train_seg = image_names[:num_train]
  val_seg = image_names[num_train:]

  split_dir = os.path.join(img_dir, 'ImageSets/Segmentation')
  os.makedirs(split_dir)

  with open(os.path.join(split_dir, 'train.txt'), 'w') as f:
    for item in train_seg:
      f.write("%s\n" % item)

  with open(os.path.join(split_dir, 'val.txt'), 'w') as f:
    for item in val_seg:
      f.write("%s\n" % item)

  with open(os.path.join(split_dir, 'trainval.txt'), 'w') as f:
    for item in image_names:
      f.write("%s\n" % item)


def sly_omnivor_convert_pascalvoc_to_backmatting_dataset(voc_dir, bg_dir, out_dir):
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)

  img_dir = os.path.join(voc_dir, 'JPEGImages')
  seg_dir = os.path.join(voc_dir, 'SegmentationClass')
  mapping_file = os.path.join(voc_dir, 'mapping.txt')
  out_train_file = os.path.join(out_dir, 'adobe_train_data.csv')

  out_train_lines = []
  idx_to_img_name = {}
  with open(mapping_file, 'r') as mf:
    for line in mf:
      parts = line.strip().split('\t')
      idx_to_img_name[parts[0]] = parts[1]

  for img_path in glob.glob(img_dir + '/*.jpg'):
    img_name = os.path.basename(img_path)
    img_idx = os.path.splitext(img_name)[0]
    name_and_bg = idx_to_img_name[img_idx].split('_CV')
    if len(name_and_bg) <= 1: # omnivor test images don't have background, skip
      continue

    img = cv2.imread(img_path)
    h, w = img.shape[:-1]

    # save composite image as *_comp.png
    out_comp_path = os.path.join(out_dir, img_idx + '_comp.png')
    cv2.imwrite(out_comp_path, img)

    # save background image as *_back.png
    original_img_name = idx_to_img_name[img_idx]
    bg_name = 'CV' + original_img_name.split('_CV')[1]
    bg_path = os.path.join(bg_dir, bg_name)
    im_back = cv2.imread(bg_path)
    im_back = cv2.resize(im_back, (w, h))
    out_bg_path = os.path.join(out_dir, img_idx + '_back.png')
    cv2.imwrite(out_bg_path, im_back)

    # save mask (alpha) image as *_alpha.png
    alpha_path = os.path.join(seg_dir, img_idx + '.png')
    alpha = cv2.imread(alpha_path, 0)
    out_alpha_path = os.path.join(out_dir, img_idx + '_alpha.png')
    cv2.imwrite(out_alpha_path, alpha)

    # save foreground image as *_fore.png
    im_fore = cv2.bitwise_and(img, img, mask = alpha)
    out_fore_path = os.path.join(out_dir, img_idx + '_fore.png')
    cv2.imwrite(out_fore_path, im_fore)

    line = 'Data_adobe/{};Data_adobe/{};Data_adobe/{};Data_adobe/{}\n'.format(
      os.path.basename(out_fore_path), 
      os.path.basename(out_alpha_path), 
      os.path.basename(out_comp_path), 
      os.path.basename(out_bg_path))
    print(line)
    out_train_lines.append(line)

  with open(out_train_file, 'w') as otff:
    for l in out_train_lines:
      otff.write(l)


# extract_omnivor_voc12_foreground(OMNIVOR_SUBJECT_DIR, 'c:/Users/lhoang/Downloads/tmp/omnivor/')

# sly_omnivor_convert_pascalvoc_to_backmatting_dataset(
#   OMNIVOR_SUPERVISELY_PASCALVOC_DIR,
#   OMNIVOR_BACKGROUND_DIR, BACK_MATTING_DIR)

# sly_show_img_mask_contours('ds6', 'adult-bag-bags-buy-41277.jpeg')

# sly_generate_foreground_images()
# create_omnivor_foreground_and_rawseg_images(OMNIVOR_SUPERVISELY_PASCALVOC_DIR)
# create_omnivor_foreground_and_rawseg_images(OMNIVOR_SUPERVISELY_PASCALVOC_SMALLFG_DIR)

# slyomnivor_convert_mask_to_voc21class(OMNIVOR_SUPERVISELY_PASCALVOC_DIR + '/SegmentationClass',
#   OMNIVOR_SUPERVISELY_PASCALVOC_DIR + '/SegmentationClassRaw')

# voc12omnivor_convert_mask_to_voc21class(OMNIVOR_VOC2012_DIR + '/SegmentationClass',
#   OMNIVOR_VOC2012_DIR + '/SegmentationClassRaw')

# create_voc12_slyper_omnivor_data_splits(OMNIVOR_VOC2012_SLYPER_DIR)

# voc12_create_dataset_new_bg(VOC_DIR, OMNIVOR_BACKGROUND_DIR, OMNIVOR_VOC2012_DIR, OMNIVOR_SUBJECT_DIR)
create_omnivor_person_segmentation_dataset()
# extract_omnivor_foreground(OMNIVOR_SUBJECT_DIR, 
#   'c:/Users/lhoang/Downloads/tmp/omnivor/', 
#   OMNIVOR_SUPERVISELY_PASCALVOC_DIR + '/mapping.txt')
# intersect_two_foreground_images(
#   'c:/users/lhoang/dropbox/personal/company/omnivor/background-matting/benchmarks/fg_segmented',
#   'c:/users/lhoang/dropbox/personal/company/omnivor/background-matting/benchmarks/deeplabv3plus_slyomnivor_512',
#   'c:/users/lhoang/downloads/tmp/intersect_fg_segmented_deeplabv3plus_slyomnivor_512'
# )

# pascalvoc_generate_foreground_images('g:/data/datasets/VOC2012')

# compose_image_to_new_bg(
#   'G:/data/datasets/VOC2012/JPEGImages/2007_001430.jpg',
#   'G:/data/datasets/VOC2012/SegmentationClass/2007_001430.png',
#   'G:/Data/datasets/omnivor-background/CV6_17.png_rgb000000.png',
#   show_result=True,
#   resize_fg_max=1,
#   out_mask_path=None,
#   ignore_mask_value=255,
#   read_mask_with_cv2=False)