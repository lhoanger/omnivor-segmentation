from numpy.matrixlib.defmatrix import bmat
from numpy.testing._private.utils import assert_equal
import supervisely_lib as sly  # Supervisely Python SDK
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image
import cv2
from glob import glob
import os, sys
from random import shuffle
from shutil import copyfile
from tqdm import tqdm
import filecmp
import tempfile
import util


# Root directory of supervisely person dataset
SUPERVISELY_DIR = 'g:/data/datasets/supervisely-person-dataset'
# Directory containing omnivor unrectified background images
OMNIVOR_UNRECT_BACKGROUND_DIR = 'g:/data/datasets/omnivor-unrect-background'
# Directory containing omnivor rectified background images
OMNIVOR_RECT_BACKGROUND_DIR = 'g:/data/datasets/omnivor-rect-background'
# Directory containing omnivor images with subjects in studio
OMNIVOR_SUBJECT_DIR = 'g:/data/datasets/omnivor-subject-images'
# Output directory of dataset used to train background-matting model
OUT_BACKMATT_DATASET_DIR = 'g:/data/datasets/backmatting-sly-omnivor'
# Output directory of dataset used to train deeplabv3+ model
OUT_DEEPLAB_DATASET_DIR = 'g:/data/datasets/deeplab-sly-omnivor'
# file extension for the omnivor unrectified background images
OMNIVOR_UNRECT_BG_EXT = '.jpg'
# file extension for the omnivor rectified background images
OMNIVOR_RECT_BG_EXT = '.png'
# file extension required for images in the deeplab dataset
DEEPLAB_IMG_EXT = '.jpg'
# file extension required for segmentation masks in the deeplab dataset
DEEPLAB_SEG_EXT = '.png'
# file extension for the omnivor subject images
OMNIVOR_SUBJECT_EXT = '.png'
# csv file used in training background-matting model
BACK_MATTING_CSV_FILE = os.path.join(OUT_BACKMATT_DATASET_DIR, 'adobe_train_data.csv')
# Max image size to create in the dataset
MAX_IMAGE_DIM = 512
# Minimum foreground ratio required when creating new dataset, images with smaller foreground are skipped
MIN_FG_RATIO = 0.1
# Probability of using unrectified background images in composition
UNRECT_BG_SAMPLING = 0.5
# Divider used in naming the output composite image names, e.g. if set to __, output name is [dataset]__[image]__[bg]
COMP_NAME_DIVIDER = '__'


def get_sly_binary_mask(ann_path, project_meta):
  '''
  Read the annotation json and return a binary mask containing the foreground subject (including both person and 
  non-person foreground). The mask is of the same size as the corresponding image.

  @param ann_path: The path to the annotation file
  @param project_meta: The metadata for the supervisely project
  @return: A 2d array binary image where 0 = bg and 255 = fg
  '''
  ann = sly.Annotation.load_json_file(ann_path, project_meta)
  ann_render = np.zeros(ann.img_size + (3,), dtype=np.uint8)
  ann.draw(ann_render)
  
  # supervisely person dataset has 3 labels: person-bmp, person-polygon, and neutral
  # neutral contains objects such as backpack etc... that the person is holding
  # all 3 labels have positive pixel values
  mask = ann_render[:, :, 0]
  mask[mask > 0] = 255
  return mask


def compose_image_to_new_bg(img, mask, bg, show_result = False):
  '''
  Compose the foreground of one image (using given mask) over a given background image.

  @param img: The nd-array image
  @param mask: The 2d-array binary mask image
  @param bg: The nd-array background image
  @param show_result: Whether to show the composite image (useful for debugging)
  @return: A tuple of (foreground, composite) each containing an nd-array image 
  '''
  assert_equal(img.shape, bg.shape, 'image and background must have the same size')
  assert_equal(img.shape[:-1], mask.shape, 'image and mask must have the same dimensions')

  fore = cv2.bitwise_and(img, img, mask = mask)
  im_mask_inv = cv2.bitwise_not(mask)
  back = cv2.bitwise_and(bg, bg, mask = im_mask_inv)
  comp = cv2.add(fore, back)

  if show_result:
    cv2.imshow('composite', comp)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

  return fore, comp


def create_slyomnivor_dataset(sly_dir, unrect_bg_dir, rect_bg_dir, out_dir):
  '''
  Create a new dataset by composing the supervisely-person foreground images over omnivor studio backgrounds. The output
  dataset contains a set of 4 images [_comp.png, _fore.png, _alpha.png, _back.png] that are composite, foreground, mask,
  and background images respectively. 

  @param sly_dir: The root directory of the supervisely person dataset that should be structured as follows:
    - Root Directory
      + meta.json: json file listing the metadata of the project such as the set of labels and color maps
      + ds1:
        * img: folder containing original images with subjects
        * ann: folder containing .json annotation for each image
      + ds2:
        ...
  @param unrect_bg_dir: The directory containing unrectifed background images
  @param rect_bg_dir: The directory containing rectified background images
  @param out_dir: The output directory for the supervisely-omnivor dataset
  '''
  unrect_bg_images = glob(unrect_bg_dir + '/*' + OMNIVOR_UNRECT_BG_EXT)
  rect_bg_images = glob(rect_bg_dir + '/*' + OMNIVOR_RECT_BG_EXT)

  if not os.path.exists(out_dir):
    os.mkdir(out_dir)

  project = sly.Project(sly_dir, sly.OpenMode.READ)
  for dsk in project.datasets:
    ds = project.datasets.get(dsk.name)
    for img_name in ds:
      print('Processing {}/{}'.format(dsk.name, img_name))
      item_paths = ds.get_item_paths(img_name)

      # read original image and resize
      img = cv2.imread(item_paths.img_path)
      img = util.resize_image_to_max_size(img, MAX_IMAGE_DIM, MAX_IMAGE_DIM)
      h,w = img.shape[:-1]

      # read binary mask and resize
      mask = get_sly_binary_mask(item_paths.ann_path, project.meta)
      mask = cv2.resize(mask, (w, h), interpolation=cv2.INTER_NEAREST)
      fg_ratio = np.sum(mask == 255) / mask.size
      if fg_ratio < MIN_FG_RATIO:
        print('Skipped, too little foreground (fg_ratio = {})'.format(fg_ratio))
        continue

      # read background image and resize
      if np.random.rand() <= UNRECT_BG_SAMPLING:
        bg_path = unrect_bg_images[np.random.randint(0, len(unrect_bg_images))]
      else:
        bg_path = rect_bg_images[np.random.randint(0, len(rect_bg_images))]
      bg = cv2.imread(bg_path)
      bg = cv2.resize(bg, (w, h))

      # extract foreground and get composite image
      fore, comp = compose_image_to_new_bg(img, mask, bg)

      # save images
      img_base_name = '{0}{3}{1}{3}{2}'.format(dsk.name, os.path.splitext(img_name)[0],
        os.path.splitext(os.path.basename(bg_path))[0], COMP_NAME_DIVIDER)
      mask_path = os.path.join(out_dir, img_base_name + '_alpha.png')
      back_path = os.path.join(out_dir, img_base_name + '_back.png')
      fore_path = os.path.join(out_dir, img_base_name + '_fore.png')
      comp_path = os.path.join(out_dir, img_base_name + '_comp.png')
      cv2.imwrite(mask_path, mask)
      cv2.imwrite(back_path, bg)
      cv2.imwrite(fore_path, fore)
      cv2.imwrite(comp_path, comp)


def create_backmatting_train_csv(backmatt_dir, out_csv_file):
  '''
  Create the csv file used for training the background-matting model, each line contains {fg;alpha;comp;back}.

  @param backmatt_dir: The directory for the supervisely-omnivor dataset
  @param out_csv_file: The output csv file to create
  '''
  comp_images = glob(backmatt_dir + '/*_comp.png')
  fore_images = glob(backmatt_dir + '/*_fore.png')
  back_images = glob(backmatt_dir + '/*_back.png')
  mask_images = glob(backmatt_dir + '/*_alpha.png')

  lines = []
  for i in range(len(comp_images)):
    lines.append('Data_adobe/{};Data_adobe/{};Data_adobe/{};Data_adobe/{}\n'.format(os.path.basename(fore_images[i]),
      os.path.basename(mask_images[i]), os.path.basename(comp_images[i]), os.path.basename(back_images[i])))

  with open(out_csv_file, 'w') as cf:
    for l in lines:
      cf.write(l)


def create_deeplab_dataset(backmatt_dir, out_deeplab_dir, omnivor_subject_dir):
  '''
  Create the dataset used for training deeplabv3+ mode, which has the same structure as the PASCAL VOC 2012 dataset:
    + JPEGImages: folder containing .jpg images
    + SegmentationClass: folder containing .png segmentation images, containing {0,255} mask values
    + ImageSets/Segmentation/*.txt: text files listing the names of image files used for each dataset split. This code
      also creates a omnivor.txt split for testing on real studio images with subjects
    + Foreground: fg images are not needed by the training process, but are useful for visual testing to make sure the
      masks produce the correct foreground

  @param backmatt_dir: The directory for the background-matting dataset
  @param out_deeplab_dir: The directory for the deeplab dataset
  @param omnivor_subject_dir: The directory for the omnivor subject images
  '''
  print('Creating deeplab dataset')
  out_img_dir = os.path.join(out_deeplab_dir, 'JPEGImages')
  out_fore_dir = os.path.join(out_deeplab_dir, 'Foreground')
  out_seg_dir = os.path.join(out_deeplab_dir, 'SegmentationClass')
  out_split_dir = os.path.join(out_deeplab_dir, 'ImageSets/Segmentation')
  os.makedirs(out_img_dir)
  os.makedirs(out_fore_dir)
  os.makedirs(out_seg_dir)
  os.makedirs(out_split_dir)

  print('Creating main dataset split')
  trainval_image_names = []
  comp_images = glob(backmatt_dir + '/*_comp.png')
  for ci in tqdm(comp_images):
    ci_name = os.path.basename(ci)
    # write image out in .jpg format
    img = cv2.imread(ci)
    out_img_path = os.path.join(out_img_dir, ci_name.replace('_comp.png', DEEPLAB_IMG_EXT))
    cv2.imwrite(out_img_path, img)
    # copy foreground
    fore_path = ci.replace('_comp.png', '_fore.png')
    copyfile(fore_path, os.path.join(out_fore_dir, os.path.basename(fore_path).replace('_fore.png', DEEPLAB_SEG_EXT)))
    # copy segmentation mask
    seg_path = ci.replace('_comp.png', '_alpha.png')
    copyfile(seg_path, os.path.join(out_seg_dir, os.path.basename(seg_path).replace('_alpha.png', DEEPLAB_SEG_EXT)))
    # add to train/val list
    trainval_image_names.append(ci_name.replace('_comp.png', ''))
  
  # write trainval split file
  with open(os.path.join(out_split_dir, 'trainval.txt'), 'w') as tvf:
    for l in trainval_image_names:
      tvf.write(l + '\n')

  print('Creating omnivor dataset split')
  # copy omnivor subject images into the dataset, with dummy segmentation mask
  omnivor_image_names = []
  subjects = glob(omnivor_subject_dir + '/*' + OMNIVOR_SUBJECT_EXT)
  for simg_path in tqdm(subjects):
    # write image
    simg_name = os.path.splitext(os.path.basename(simg_path))[0]
    simg = cv2.imread(simg_path)
    simg = util.resize_image_to_max_size(simg, MAX_IMAGE_DIM, MAX_IMAGE_DIM)
    cv2.imwrite(os.path.join(out_img_dir, simg_name + DEEPLAB_IMG_EXT), simg)
    # write dummy segmentation
    dummy_seg = np.zeros_like(simg[:,:,0]).astype(np.uint8)
    cv2.imwrite(os.path.join(out_seg_dir, simg_name + DEEPLAB_SEG_EXT), dummy_seg)
    # add to dataset split
    omnivor_image_names.append(simg_name)

  # write omnivor split file
  with open(os.path.join(out_split_dir, 'omnivor.txt'), 'w') as omf:
    for l in omnivor_image_names:
      omf.write(l + '\n')


def extract_fg_from_deeplab_mask(original_img_dir, mask_dir):
  '''
  Extract foreground images from the deeplab predicted masks.

  @param original_img_dir: The directory containing original images to extract foreground from
  @param mask_dir: The directory containing the predicted masks
  @remark: The mask files are named "b'file_name'.png" by the deeplab code
  '''
  mask_files = glob(mask_dir + '/b*.png')

  for i in range(len(mask_files)):
    file_name = os.path.basename(mask_files[i])[2:-5] + OMNIVOR_SUBJECT_EXT
    original_img_path = os.path.join(original_img_dir, file_name)
    img = cv2.imread(original_img_path)
    mask = cv2.imread(mask_files[i], 0)
    h,w = img.shape[:-1]

    # Test: mask values must be 21-class
    mask_values = np.unique(mask)
    for v in mask_values:
      assert v >= 0 and v < 21, 'Predicted mask value must be 21-class, invalid value found: {}'.format(v)

    mask = cv2.resize(mask.astype(np.uint8), (w, h))
    mask_sel = (mask == 15).astype(np.float32)
    mask = (255 * mask_sel).astype(np.uint8)
    fg = cv2.bitwise_and(img, img, mask = mask)
    fg_path = os.path.join(mask_dir, file_name)
    cv2.imwrite(fg_path, fg)

    # write mask image to be used as soft segmentation in back-matting model
    cv2.imwrite(fg_path.replace('.png', '_masksDL.png'), mask)


def run_tests(sly_dir, unrect_bg_dir, rect_bg_dir, backmatt_dir, backmatt_csv_file, deeplab_dir):
  '''
  Test supervisely dataset, back-matting dataset, and deeplab dataset for various correctness conditions.

  @param sly_dir: The root directory of the supervisely person dataset
  @param unrect_bg_dir: The directory containing omnivor unrectified background images
  @param rect_bg_dir: The directory containing omnivor rectified background images
  @param backmatt_dir: The directory for the back-matting dataset
  @param backmatt_csv_file: The csv file used to train back-matting model
  @param deeplab_dir: The directory of the deeplab dataset
  '''
  bg_images = glob(unrect_bg_dir + '/*' + OMNIVOR_UNRECT_BG_EXT) + glob(rect_bg_dir + '/*' + OMNIVOR_RECT_BG_EXT)
  actual_bg_names = set()
  print('------------------------------------------')
  print('------------------------------------------')
  print('Test: omnivor background images')
  unique_bg_names = set()
  # Test: background image names are unique across the rectified and unrectified sets
  for p in bg_images:
    bg_file_name = os.path.basename(p)
    assert bg_file_name not in unique_bg_names, 'Background image names are not unique: {}'.format(bg_file_name)
    unique_bg_names.add(bg_file_name)
  print('PASSED')

  project = sly.Project(sly_dir, sly.OpenMode.READ)

  print('------------------------------------------')
  print('------------------------------------------')
  print('Test: supervisely person dataset')
  sly_img_names = set()
  for dsk in project.datasets:
    ds = project.datasets.get(dsk.name)
    for img_name in ds:
      # Test: image names are unique globally
      name = os.path.splitext(img_name)[0]
      assert name not in sly_img_names, 'Image names in supervisely person dataset are not unique'
      sly_img_names.add(name)
  print('PASSED')

  print('------------------------------------------')
  print('------------------------------------------')
  print('Test: background-matting dataset')
  comp_images = glob(backmatt_dir + '/*_comp.png')
  fore_images = glob(backmatt_dir + '/*_fore.png')
  back_images = glob(backmatt_dir + '/*_back.png')
  mask_images = glob(backmatt_dir + '/*_alpha.png')
  for i in tqdm(range(len(comp_images))):
    comp = cv2.imread(comp_images[i])
    fore = cv2.imread(fore_images[i])
    back = cv2.imread(back_images[i])
    mask = cv2.imread(mask_images[i], 0)

    # Test: sufficient foreground
    fg_ratio = np.sum(mask == 255) / mask.size
    assert fg_ratio >= MIN_FG_RATIO, 'Image {} has insufficient foreground (ratio {})'.format(
      mask_images[i], fg_ratio)
    
    # Test: all (comp, fore, back, alpha) images in the sly-omnivor dataset are of the same size
    assert_equal(comp.shape, fore.shape, 'Size mismatch for composite and foreground image {}'.format(comp_images[i]))
    assert_equal(comp.shape, back.shape, 'Size mismatch for composite and background image {}'.format(comp_images[i]))
    assert_equal(comp.shape[:-1], mask.shape, 'Size mismatch for composite and mask image {}'.format(comp_images[i]))

    # Test: max image dimension
    assert np.max(comp.shape) <= MAX_IMAGE_DIM, 'Image {} has dimension {}, must be no greater than {}'.format(
      comp_images[i], comp.shape, MAX_IMAGE_DIM)

    # Test: mask images only have {0,255} values
    mask_values = np.unique(mask)
    for v in mask_values:
      assert v in {0, 255}, 'Invalid mask value {}, must be 0 or 255'.format(v)

    for c in range(fore.shape[2]):
      # Test: foreground region matches exactly between fore and comp images
      assert np.array_equal(fore[:,:,c][mask==255], comp[:,:,c][mask==255]), \
        'Foreground mismatch for fore and comp images {}'.format(comp_images[i])

      # Test: non-foreground region of fore image is all black
      assert not np.any(fore[:,:,c][mask==0]),\
        'Non-foreground region of fore image contains non-black values {}'.format(fore_images[i])

      # Test: background region matches exactly between back and comp images
      assert np.array_equal(back[:,:,c][mask==0], comp[:,:,c][mask==0]),\
        'Background mismatch for back and comp images {}'.format(comp_images[i])

    # Test: file names match and are constructed properly
    assert_equal(comp_images[i].replace('_comp.png', ''), fore_images[i].replace('_fore.png', ''))
    assert_equal(comp_images[i].replace('_comp.png', ''), back_images[i].replace('_back.png', ''))
    assert_equal(back_images[i].replace('_back.png', ''), mask_images[i].replace('_alpha.png', ''))
    names = os.path.basename(comp_images[i]).replace('_comp.png', '').split(COMP_NAME_DIVIDER)
    ds_name = names[0]
    img_name = names[1]
    
    # Test: original supervisely image must exist
    img_path = os.path.join(sly_dir, ds_name, 'img', img_name)
    assert len(glob(img_path + '*')) > 0, \
      'Incorrect file name {}, supervisely image {} not found'.format(os.path.basename(comp_images[i]), img_path)
    
    # Test: original background image must exist
    bg_name = names[2]
    bg_path = os.path.join(unrect_bg_dir, bg_name + OMNIVOR_UNRECT_BG_EXT)
    if not os.path.exists(bg_path):
      bg_path = os.path.join(rect_bg_dir, bg_name + OMNIVOR_RECT_BG_EXT)
      assert os.path.exists(bg_path), \
        'Incorrect file name {}, background image {} not found'.format(os.path.basename(comp_images[i]), bg_path)

    actual_bg_names.add(os.path.basename(bg_path))

  # Test: all background images are used in the dataset
  for bg_name in unique_bg_names:
    assert bg_name in actual_bg_names, 'Background {} is not used in the dataset'.format(bg_name)

  print('Test: background matting csv file {}'.format(backmatt_csv_file))
  with open(backmatt_csv_file, 'r') as cf:
    for line in cf:
      parts = line.strip().replace('Data_adobe/', '').split(';')
      
      # Test: the images on each line belong to the same set
      assert_equal(parts[0].replace('_fore.png', ''), parts[1].replace('_alpha.png', ''))
      assert_equal(parts[1].replace('_alpha.png', ''), parts[2].replace('_comp.png', ''))
      assert_equal(parts[2].replace('_comp.png', ''), parts[3].replace('_back.png', ''))

      # Test: all images exist in the directory
      for file_name in parts:
        assert os.path.exists(os.path.join(backmatt_dir, file_name)),\
          'File {} listed in the csv does not exist in {}'.format(file_name, backmatt_dir)
  
  print('PASSED')

  print('------------------------------------------')
  print('------------------------------------------')
  print('Test: DeepLab dataset')
  # temp jpg file to save png images to so we can compare with other jpg images
  tmp_jpg_file = tempfile.NamedTemporaryFile(suffix=DEEPLAB_IMG_EXT).name
  # Test: main trainval split
  trainval_img_names = []
  with open(os.path.join(deeplab_dir, 'ImageSets/Segmentation/trainval.txt'), 'r') as tvf:
    for l in tqdm(tvf):
      name = l.strip()
      trainval_img_names.append(name)

      # Test: image exists in both the backmatting dir and deeplab dir
      dl_path = os.path.join(deeplab_dir, 'JPEGImages', name + DEEPLAB_IMG_EXT)
      assert os.path.exists(dl_path), 'Image {} missing from deeplab dataset directory'.format(dl_path)
      bm_path = os.path.join(backmatt_dir, name + '_comp.png')
      assert os.path.exists(bm_path), 'Image {} missing from backmatt dataset directory'.format(bm_path)
      # Test: seg and fore images exist
      seg_path = os.path.join(deeplab_dir, 'SegmentationClass', name + DEEPLAB_SEG_EXT)
      assert os.path.exists(seg_path), 'Segmentation {} not found'.format(seg_path)
      fore_path = os.path.join(deeplab_dir, 'Foreground', name + DEEPLAB_SEG_EXT)
      assert os.path.exists(fore_path), 'Foreground image {} not found'.format(fore_path)

      # Test: jpg image is the same as the original png image in background-matting dataset
      bm_img = cv2.imread(bm_path)
      cv2.imwrite(tmp_jpg_file, bm_img)
      assert filecmp.cmp(dl_path, tmp_jpg_file, shallow=False),\
        'Jpg image {} in deeplab dataset does not match png from backmatt {} (temporarily saved to jpg file at {})'\
        .format(dl_path, bm_path, tmp_jpg_file)
      
      # Test: deeplab mask is the same as that in the background-matting dataset
      bm_mask_path = os.path.join(backmatt_dir, name + '_alpha.png')
      assert filecmp.cmp(seg_path, bm_mask_path, shallow=False),\
        'Mask images do not match, {} vs {}'.format(seg_path, bm_mask_path)
  
  os.remove(tmp_jpg_file)

  # Test: omnivor split
  omnivor_img_names = []
  with open(os.path.join(deeplab_dir, 'ImageSets/Segmentation/omnivor.txt'), 'r') as omf:
    for l in omf:
      name = l.strip()
      omnivor_img_names.append(name)
      # Test: image actually exists
      dl_path = os.path.join(deeplab_dir, 'JPEGImages', name + DEEPLAB_IMG_EXT)
      assert os.path.exists(dl_path), 'Image {} missing from deeplab dataset directory'.format(dl_path)
  
  # Test: all images are included in the splits
  actual_image_names = glob(os.path.join(deeplab_dir, 'JPEGImages/*' + DEEPLAB_IMG_EXT))
  actual_image_names = set([os.path.splitext(os.path.basename(p))[0] for p in actual_image_names])
  assert_equal(set(trainval_img_names + omnivor_img_names), actual_image_names)
  print('PASSED')

  print('------------------------------------------')
  print('------------------------------------------')
  print('All Tests: PASSED')


# create the output dataset containing foreground, background, alpha, and composite images
create_slyomnivor_dataset(SUPERVISELY_DIR, OMNIVOR_UNRECT_BACKGROUND_DIR, OMNIVOR_RECT_BACKGROUND_DIR,
  OUT_BACKMATT_DATASET_DIR)

# create the csv file needed for training background-matting model
create_backmatting_train_csv(OUT_BACKMATT_DATASET_DIR, BACK_MATTING_CSV_FILE)

# take the dataset above and convert into the right structure for training deeplab model
create_deeplab_dataset(OUT_BACKMATT_DATASET_DIR, OUT_DEEPLAB_DATASET_DIR, OMNIVOR_SUBJECT_DIR)

# run test suite
run_tests(SUPERVISELY_DIR, OMNIVOR_UNRECT_BACKGROUND_DIR, OMNIVOR_RECT_BACKGROUND_DIR, OUT_BACKMATT_DATASET_DIR,
  BACK_MATTING_CSV_FILE, OUT_DEEPLAB_DATASET_DIR)

# # extract foreground images from the deeplab predicted mask
# extract_fg_from_deeplab_mask(OMNIVOR_SUBJECT_DIR, 'c:/Users/lhoang/Downloads/tmp/omnivor/')
